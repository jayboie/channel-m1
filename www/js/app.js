
 

(function(){
  'use strict';
  var module = angular.module('app', ['onsen']);



 
})();

var fasttrackdb = {};
        fasttrackdb.webdb = {};
        fasttrackdb.webdb.db = null;

        fasttrackdb.webdb.open = function () {
            var dbSize = 5 * 1024 * 1024; // 5MB
            fasttrackdb.webdb.db = openDatabase("cm1", "1.0", "cm1", dbSize);
        }

        fasttrackdb.webdb.createTable = function () {
           
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                tx.executeSql("CREATE TABLE IF NOT EXISTS users(ID INTEGER PRIMARY KEY ASC, phone TEXT, fullname TEXT,address TEXT, email TEXT, gender TEXT, country TEXT, heartbeat TEXT, status TEXT,position TEXT)", []);
                 
            });
           
        }

        fasttrackdb.webdb.createTablePosts = function () {
           
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                tx.executeSql("CREATE TABLE IF NOT EXISTS posts(ID INTEGER PRIMARY KEY ASC,serverdb_id TEXT, picname TEXT, title TEXT, notes TEXT, type TEXT)", []);
                 
            });
           
        }

        fasttrackdb.webdb.createTableEvents = function () {
           
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                tx.executeSql("CREATE TABLE IF NOT EXISTS events(ID INTEGER PRIMARY KEY ASC,serverdb_id TEXT, picname TEXT, title TEXT, notes TEXT, type TEXT)", []);
                 
            });
           
        }

        fasttrackdb.webdb.createTableShop = function () {
           
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                tx.executeSql("CREATE TABLE IF NOT EXISTS shop(ID INTEGER PRIMARY KEY ASC,serverdb_id TEXT, shopimage TEXT, shoptype TEXT, description TEXT)", []);
                 
            });
           
        }

        fasttrackdb.webdb.createTableShopClients = function () {
           
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                tx.executeSql("CREATE TABLE IF NOT EXISTS shopclients(ID INTEGER PRIMARY KEY ASC,serverdb_id TEXT, shop_id TEXT, companyname TEXT, slogan TEXT, description TEXT, logo TEXT, email TEXT, phoneno TEXT, website TEXT)", []);
                 
            });
           
        }

        fasttrackdb.webdb.createTablePrograms = function () {
           
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                tx.executeSql("CREATE TABLE IF NOT EXISTS programs(ID INTEGER PRIMARY KEY ASC,serverdb_id TEXT, programname TEXT, video1 TEXT, video2 TEXT, video3 TEXT, video4 TEXT, video5 TEXT, programimage TEXT, description TEXT)", []);
                 
            });
           
        }

        fasttrackdb.webdb.addShopClients = function (serverdb_id,shop_id,companyname,slogan,description,logo,email,phoneno,website) {
            //alert("");
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                
                tx.executeSql("INSERT INTO shopclients(serverdb_id,shop_id,companyname,slogan,description,logo,email,phoneno,website) VALUES (?,?,?,?,?,?,?,?,?)",
                    [serverdb_id,shop_id,companyname,slogan,description,logo,email,phoneno,website],
                    fasttrackdb.webdb.onSuccess,
                    fasttrackdb.webdb.onError
                    );
            });
        }

        fasttrackdb.webdb.updateShopClients = function (serverdb_id,shop_id,companyname,slogan,description,logo,email,phoneno,website,id) {
            //alert("");
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                
                tx.executeSql("UPDATE shopclients set companyname = '"+companyname+"', slogan = '"+slogan+"', description = '"+description+"', logo = '"+logo+"', email = '"+email+"', phoneno = '"+phoneno+"', website='"+website+"' where ID = "+id,
                    fasttrackdb.webdb.onSuccess,
                    fasttrackdb.webdb.onError
                    );
            });
        }
 

        fasttrackdb.webdb.updateShop = function (serverdb_id,shopimage,shoptype,description,shop_id) {
            //alert("");
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                
                tx.executeSql("UPDATE shop set serverdb_id = '"+serverdb_id+"', shopimage = '"+shopimage+"', description = '"+description+"', shoptype = '"+shoptype+"' where serverdb_id = "+shop_id,
                    fasttrackdb.webdb.onSuccess,
                    fasttrackdb.webdb.onError
                    );
            });
        }
         
        

        fasttrackdb.webdb.addPost = function (serverdb_id,picname,title,notes,type) {
             
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                
                tx.executeSql("INSERT INTO posts(serverdb_id,picname,title,notes,type) VALUES (?,?,?,?,?)",
                    [serverdb_id,picname,title,notes,type],
                    fasttrackdb.webdb.onSuccess,
                    fasttrackdb.webdb.onError
                    );
            });
        }

        fasttrackdb.webdb.addEvent = function (serverdb_id,picname,title,notes,type) {
             
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                
                tx.executeSql("INSERT INTO events(serverdb_id,picname,title,notes,type) VALUES (?,?,?,?,?)",
                    [serverdb_id,picname,title,notes,type],
                    fasttrackdb.webdb.onSuccess,
                    fasttrackdb.webdb.onError
                    );
            });
        }



        fasttrackdb.webdb.addShop = function (serverdb_id,shopimage,shoptype,description) {
            //alert("");
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                
                tx.executeSql("INSERT INTO shop(serverdb_id,shopimage,shoptype,description) VALUES (?,?,?,?)",
                    [serverdb_id,shopimage,shoptype,description],
                    fasttrackdb.webdb.onSuccess,
                    fasttrackdb.webdb.onError
                    );
            });
        }

        

        fasttrackdb.webdb.addProgram = function (serverdb_id,programname,video1,video2,video3,video4,video5,programimage,description) {
            //alert("");
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                
                tx.executeSql("INSERT INTO programs(serverdb_id,programname,video1,video2,video3,video4,video5,programimage,description) VALUES (?,?,?,?,?,?,?,?,?)",
                    [serverdb_id,programname,video1,video2,video3,video4,video5,programimage,description],
                    fasttrackdb.webdb.onSuccess,
                    fasttrackdb.webdb.onError
                    );
            });
        }

        //msg[i].id,msg[i].programname,msg[i].video1,msg[i].video2,msg[i].video3,msg[i].video4,msg[i].video5,msg[i].programimage,programs[idd].id

        fasttrackdb.webdb.updateProgram = function (serverdb_id,programname,video1,video2,video3,video4,video5,programimage,description,id) {
            //alert("");
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                
                tx.executeSql("UPDATE programs set programname = '"+programname+"', video1 = '"+video1+"', video2 = '"+video2+"', video3 = '"+video3+"', video4 = '"+video4+"', video5 = '"+video5+"', programimage='"+programimage+"', description ='"+description+"' where ID = "+id,
                    fasttrackdb.webdb.onSuccess,
                    fasttrackdb.webdb.onError
                    );
            });
        }



        fasttrackdb.webdb.deleteProgram = function () {
            //alert("");
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                
                tx.executeSql("update programs set programname='moyin' where ID=3",
                    fasttrackdb.webdb.onSuccess,
                    fasttrackdb.webdb.onError
                    );
            });
        }


        fasttrackdb.webdb.onError=function(e){
         // alert(e);
        }





 
  dbresult=[];
  dblength=0;
  dbitems=[];
  forum=[];
  programs=[];




function getPrograms(query,callback){ // <-- extra param
 // dbresult="";
    
    var db = fasttrackdb.webdb.db;
   db.transaction(function (tx) {
      tx.executeSql(query, [], function(tx, rs){
        dblength=rs.rows.length;
        for(i=0; i<rs.rows.length; i++){
          var row = rs.rows.item(i);
          programs[i]={
            id:row['ID'],
            serverdb_id:row['serverdb_id'],
            programname:row['programname'],
            video1:row['video1'],
            video2:row['video2'],
            video3:row['video3'],
            video4:row['video4'],
            video5:row['video5'],
            programimage:row['programimage'],
            description:row['description']
             
           
          }
        }
      //console.log(dbresult);
     // dbresult=dbitems;
        callback(programs); 
      });
   });
   
}


function getPosts(query,callback){ // <-- extra param
 // dbresult="";
    console.log(query);
    var db = fasttrackdb.webdb.db;
   db.transaction(function (tx) {
      tx.executeSql(query, [], function(tx, rs){
        dblength=rs.rows.length;
        for(i=0; i<rs.rows.length; i++){
          var row = rs.rows.item(i);
          dbitems[i]={
            id:row['serverdb_id'],
            title:row['title'],
            notes:row['notes'],
            type:row['type'],
            picname:row['picname']
             
           
          }
        }
      //console.log(dbresult);
     // dbresult=dbitems;
        callback(dbitems); 
      });
   });
   
}

events=[];

function getEvents(query,callback){ // <-- extra param
 // dbresult="";
    console.log(query);
    var db = fasttrackdb.webdb.db;
   db.transaction(function (tx) {
      tx.executeSql(query, [], function(tx, rs){
        dblength=rs.rows.length;
        for(i=0; i<rs.rows.length; i++){
          var row = rs.rows.item(i);
          events[i]={
            id:row['serverdb_id'],
            title:row['title'],
            notes:row['notes'],
            type:row['type'],
            picname:row['picname']
             
           
          }
        }
      //console.log(dbresult);
     // dbresult=dbitems;
        callback(dbitems); 
      });
   });
   
}


shop=[];

last_serverid=0;

function getShop(query,callback){ // <-- extra param
 // dbresult="";
    console.log(query);
    var db = fasttrackdb.webdb.db;
   db.transaction(function (tx) {
      tx.executeSql(query, [], function(tx, rs){
        dblength=rs.rows.length;
        for(i=0; i<rs.rows.length; i++){
          var row = rs.rows.item(i);
          shop[i]={
            id:row['ID'],
            serverdb_id:row['serverdb_id'],
            shoptype:row['shoptype'],
            description:row['description'],
             
            shopimage:row['shopimage']
             
           
          }
        }
      //console.log(dbresult);
     // dbresult=dbitems;
        callback(shop); 
      });
   });
   
} 

clients=[];

 
function getShopClient(query,callback){ // <-- extra param
 // dbresult="";
    console.log(query);
    var db = fasttrackdb.webdb.db;
   db.transaction(function (tx) {
      tx.executeSql(query, [], function(tx, rs){
        dblength=rs.rows.length;
        for(i=0; i<rs.rows.length; i++){
          var row = rs.rows.item(i);
          clients[i]={
            id:row['ID'],
            serverdb_id:row['serverdb_id'],
            shop_id:row['shop_id'],
            companyname:row['companyname'],
            slogan:row['slogan'],
            description:row['description'],
            logo:row['logo'],
            email:row['email'],
            phoneno:row['phoneno'],
            website:row['website']
           
          }
        }
      //console.log(dbresult);
     // dbresult=dbitems;
        callback(clients); 
      });
   });
   
} 

  

var ROOT = "http://localhost/mobileapps/cm1/dashboard/adminusers/";
//var ROOT ="http://www.colonytechnologies.com/cm1/dashboard/adminusers/";
var base_url="http://www.colonytechnologies.com/cm1/dashboard";
//var base_url="http://localhost/mobileapps/cm1/dashboard";
//var ROOT = "http://shsakure.com/admin";
API = ROOT;
pinbox = "";
var model = new Iugo({
    firstname: "",
    fullname: "",
    email:"",
    aop:"",
    has_attend:"",
    not_attend:"",
    total_cases:"",
     patientname:""
});

function loadjscssfile(filename, filetype){
 if (filetype=="js"){ //if filename is a external JavaScript file
  var fileref=document.createElement('script')
  fileref.setAttribute("type","text/javascript")
  fileref.setAttribute("src", filename)
 }
 else if (filetype=="css"){ //if filename is an external CSS file
  var fileref=document.createElement("link")
  fileref.setAttribute("rel", "stylesheet")
  fileref.setAttribute("type", "text/css")
  fileref.setAttribute("href", filename)
 }
 if (typeof fileref!="undefined")
  document.getElementsByTagName("head")[0].appendChild(fileref)
}



function loadPhone()
{
  if ($('#register_phone').val().length  == 0)
  {
    $('#register_phone').val('+234');
  }
}

function loadPhone2()
{
  if ($('#login_phone').val().length  == 0)
  {
    $('#login_phone').val('+234');
  }
}

function formatToNaira(num) {
      
      
    var p = num.toFixed(2).split(".");
    return  p[0].split("").reverse().reduce(function(acc, num, i, orig) {
        return   num + (i && !(i % 3) ? "," : "") + acc;
    }, "") + "." + p[1];
}

function switchMenu(page)
{
  console.log(page);
  
  app.menu.setMainPage(page);
  app.menu.closeMenu();
  app.menu.off("postclose", function() {

   
  
});


  app.menu.on("postclose", function() {
 
if (page=="transactions.html") {
  setTimeout(getTransactions,1000);

  //alert('');
};

}); 

}

function showSignup(){
  main_navigator.pushPage('signup.html');
}

function hideForm(){
   $("#userform").hide();
   $("#medicalform").show();
}

function showForm(){
    $("#userform").show();
   $("#medicalform").hide();
}

alternate=0;

function alternateForm(){
  if(alternate==0){
    showForm();
    alternate=1;
  }else{
    alternate=0;
    hideForm();
  }
  
}



function pullArticles(){
  getPosts("select * from posts", function(callback){
    id=0;
    for(i=0;i<dbitems.length; i++){
      id=dbitems[i].id;
    }
    if(id==""){
      id=0;
    }

  $("#loadmore").text("Loading...");
  var request = $.ajax({
  url: API + '/getPosts/',
  type: "POST",
  timeout:30000,
   data: {
          id:id
         }
  
});

  
  request.done(function( msg ) {
    $("#loadmore").text("Load more");
    unspin();
     
    msg=jQuery.parseJSON(msg);
    console.log(msg);

    if(msg.length > 0){
    
    for(i=0; i<msg.length; i++){
      
    fasttrackdb.webdb.addPost(msg[i].id,msg[i].picname,msg[i].post_title,msg[i].post_notes,msg[i].post_category);
    }
     
    }

    showArticles();
     
  
});

request.error(function( msg ) {
  $("#loadmore").text("Loading more");
  unspin();
  console.log(msg);
  alert('check internet connection');
});
});

}


function pullEvents(){
  getEvents("select * from events", function(callback){
    id=0;
    for(i=0;i<events.length; i++){
      id=events[i].id;
    }
    if(id==""){
      id=0;
    }

  $("#loadmore").text("Loading...");
  var request = $.ajax({
  url: API + '/getEvents/',
  type: "POST",
  timeout:30000,
   data: {
          id:id
         }
  
});

  
  request.done(function( msg ) {
    $("#loadmore").text("Load more");
    unspin();
     
    msg=jQuery.parseJSON(msg);
    console.log(msg);

    if(msg.length > 0){
    
    for(i=0; i<msg.length; i++){
      
    fasttrackdb.webdb.addEvent(msg[i].id,msg[i].picname,msg[i].post_title,msg[i].post_notes,msg[i].post_category);
    }
     
    }

    showEvents();
     
  
});

request.error(function( msg ) {
  $("#loadmore").text("Loading more");
  unspin();
  console.log(msg);
  alert('check internet connection');
});
});

}


function showEvents(){

  $(".timeline").html(""); 
      
    setTimeout(function(){
    getEvents("select * from events", function(callback){
    
    console.log(events);

    if(events.length==0){
     $(".timeline").append("<ons-list-item modifier='inset' class='timeline-li ng-scope list__item ons-list-item-inner list__item--tappable'>No Trends Available</ons-list-item>");
     return;
    }
   

    for(i=0; i<events.length; i++){ 

      id=events[i].id;
      
    if(i % 2 ==0){
      nstatus+=1;
        iconn='<i class="fa fa-ambulance fa-2x status0"></i>';
    }else{
        hstatus+=1;
        iconn='<i class="fa fa-check fa-2x status1"></i>';
    }

      picture=(events[i].picname);
      //alert(picture); 

      $(".timeline").append('<ons-list-item onclick="eventdetails('+i+')" class="timeline-li ng-scope list__item ons-list-item-inner list__item--tappable" modifier="tappable" ><ons-row class="row ons-row-inner"><ons-col width="50px" class="col ons-col-inner" style="-webkit-box-flex: 0; flex: 0 0 50px; max-width: 50px;"><img  class="timeline-image" src="'+picture+'"/></ons-col><ons-col class="col ons-col-inner"><div class="timeline-date">'+iconn+'</div><div class="timline-from"><span class="timeline-name"><b>'+events[i].title+'</span><span class="timeline-id">'+'  '+events[i].type+'</span></div><div class="timeline-message">articletype Type: <i>'+events[i].type+'</i></div></ons-col></ons-row></ons-list-item>');
    
    }

  });
   },100); 
  

}


function showArticles(){

  $(".timeline").html(""); 
      
      setTimeout(function(){
    getPosts("select * from posts", function(callback){
    
    console.log(dbitems);

    if(dbitems.length==0){
     $(".timeline").append("<ons-list-item modifier='inset' class='timeline-li ng-scope list__item ons-list-item-inner list__item--tappable'>No Trends Available</ons-list-item>");
     return;
    }
   

    for(i=0; i<dbitems.length; i++){ 

      id=dbitems[i].id;
      
     if(i % 2 ==0){
      nstatus+=1;
        iconn='<i class="fa fa-ambulance fa-2x status0"></i>';
      }else{
        hstatus+=1;
        iconn='<i class="fa fa-check fa-2x status1"></i>';
      }

      picture=(dbitems[i].picname);
      //alert(picture); 

      $(".timeline").append('<ons-list-item onclick="article('+i+')" class="timeline-li ng-scope list__item ons-list-item-inner list__item--tappable" modifier="tappable" ><ons-row class="row ons-row-inner"><ons-col width="50px" class="col ons-col-inner" style="-webkit-box-flex: 0; flex: 0 0 50px; max-width: 50px;"><img  class="timeline-image" src="'+picture+'"/></ons-col><ons-col class="col ons-col-inner"><div class="timeline-date">'+iconn+'</div><div class="timline-from"><span class="timeline-name"><b>'+dbitems[i].title+'</span><span class="timeline-id">'+'  '+dbitems[i].type+'</span></div><div class="timeline-message">articletype Type: <i>'+dbitems[i].type+'</i></div></ons-col></ons-row></ons-list-item>');
    
    }

  });
   },100); 
  

}

 

function articletype(type){
  type= type.toLowerCase();
   if(type=="music"){
       return 'music.jpg';
      }else if(type=="fashion"){
        return 'fashion.jpg';
      }else if(type=="lifestyle"){
        return 'lifestyle.jpg';
      }else if(type=="coding"){
        return 'coding.jpg';
      }else if(type=="sports"){
        return 'sports.jpg';
      }else{
       return 'ambulance.png';
      }
}

function article(id){
   idd=id;
  main_navigator.pushPage('case.html');
 // setTimeout(articleinit,100);
   setTimeout(function(){
     model.patientname=dbitems[id].title;
     model.case_content=dbitems[id].notes;
     $("#usermap").hide();
  
     picture =(dbitems[id].picname);
     $("#details").append(dbitems[id].notes);

     $(".case_status").append('<img style="height:200px;" src="'+picture+'"/>');
   },100);
  
}

function eventdetails(id){
   idd=id;
  main_navigator.pushPage('eventdetails.html');
  setTimeout(function(){
     model.patientname=events[id].title;
     model.case_content=events[id].notes;
     $("#usermap").hide();
  
     picture =(events[id].picname);
     $("#details").append(events[id].notes);

     $(".case_status").append('<img style="height:200px;" src="'+picture+'"/>');
   },100);
  //setTimeout(articleinit,100);
  
}

function articleinit(){
  model.patientname=dbitems[idd].title;
  model.case_content=dbitems[idd].notes;
  $("#usermap").hide();
  
  picture =(dbitems[idd].picname);
 $("#details").append(dbitems[idd].notes);

  $(".case_status").append('<img style="height:200px;" src="'+picture+'"/>');
}
 



function register()
{
      
      
            
  
    var firstname =  ( $('input#register_firstname').val());
    var lastname =  ( $('input#register_lastname').val());
    var email =  ( $('input#register_email').val());
     var phone =  ( $('input#register_phone').val());
     var gender = $('#gender').val();
     var username=$('input#username').val();
    

    if (firstname == "") {
      showalert("Please Enter Firstname.");
      $('input#register_firstname').focus(); 
      return
    } else if (lastname == "") {
      showalert("Please Enter Lastname");
      $('input#register_lastname').focus();
      return
    }
    else if (email == "") {
      showalert("Please Enter Email Address.");
      $('input#register_email').focus();
      return
    }
  

   else if (phone == ""  ){
      showalert("Please Enter a Valid Phone Number.");
      $('input#register_phone').focus();
      return
    }


   else if (username == ""  ){
      showalert("Please Enter a Valid Username.");
      $('input#username').focus();
      return
    }
  

  
   
    
  if( !validateEmail(email)) { 
      
      showalert("Please Enter a Valid Email Address.");
      $('input#register_email').focus();
      return
      
  }

  
  if (gender == '')
  {
    showalert('Please select your gender');
    return;
  }


  var capture_pin =  ( $('input#register_pin').val());
  var capture_pin_confirm =  ( $('input#register_pin2').val());
  
  
  if (capture_pin == "") {
      showalert("Please Enter a Valid PIN");
      $('input#register_pin').focus(); 
      return;
  }
  
  if (capture_pin.length != 5) {
      showalert("PIN must be exactly 5 digits");
      $('input#register_pin').focus(); 
      return
  }
  
  if (capture_pin_confirm != capture_pin) {
      showalert("Pins do not match. please retry");
      $('input#register_pin2').focus(); 
      return
  }
  

  
  
  var tosend = localStorage.getItem("capture_send_code");
  
  var hashpass = CryptoJS.MD5(capture_pin);
  hashpass = hashpass.toString();
  
               
 var request = $.ajax({
  url: API + 'registerSubscriber/',
  type: "POST",
   data: {
         email: email,
         phone: phone,
         firstname: firstname,
         lastname: lastname,
         password: hashpass,
       //  imei: tosend,
         gender: gender,
          
         username:username
        // secret : 'f9b45f5b3e6f1737804dc5d1f9c39202'
         }
  
});

  
request.done(function( msg ) {
// console.log(msg);
unspin();
   //main_navigator.pushPage('link.html');
   alert("Registered successfully"); 
   main_navigator.pushPage('login.html');
 // setTimeout(showactivity,200); 
  //localStorage.setItem("usertype","agent");
  
});

request.error(function( msg ) {
  unspin();
  console.log(msg);
  alert('check internet connection');
});
  
}

function showProgramss(){
  main_navigator.pushPage('programs.html');
}

function programEpisode(episode){
  if(episode=="video1"){
      $("#vidplayer").attr("src",base_url+"/programvideos/"+programs[idd].video1);
  }else if(episode=="video2"){
      $("#vidplayer").attr("src",base_url+"/programvideos/"+programs[idd].video2);
  }else if(episode=="video3"){
      $("#vidplayer").attr("src",base_url+"/programvideos/"+programs[idd].video3);
  }else if(episode=="video4"){
      $("#vidplayer").attr("src",base_url+"/programvideos/"+programs[idd].video4);
  }else if(episode=="video5"){
      $("#vidplayer").attr("src",base_url+"/programvideos/"+programs[idd].video5);
  }
}


function programLinks(id){
  has_paid=verifyPayment();

  if(!has_paid){
    return;
  }

  idd=id;

  console.log(programs[id]);

  main_navigator.pushPage('programview.html');
   setTimeout(programDescription,100);
}

function programDescription(){
  if(programs[idd].description==""){
    $(".pdesc").text("Information coming soon...")
  }else{
  $(".pdesc").text(programs[idd].description);
}
}


function account(){
  profile=jQuery.parseJSON(localStorage.profile);
  console.log(profile);

  model.fullname=profile[0].firstname+' '+profile[0].lastname;
  model.email=profile[0].email;
  model.aop="Specialises in "+profile[0].aop;
  model.has_attend=hstatus;
  model.not_attend=nstatus;
  model.total_cases=dblength;
}

nstatus=0;
hstatus=0;

function loginuser(){
  main_navigator.pushPage('user.html');
}

idd=0;
function casedetails(id){

  idd=id;
  main_navigator.pushPage('case.html');
  setTimeout(caseinit,100);
 
   
}

function forumdetails(id){
  idd=id;
  console.log("forum id "+idd);
   main_navigator.pushPage('message.html');
   setTimeout(pullmessage,100);
}

function pullmessage(){
  $("messages").html("");
  var request = $.ajax({
  url: API + '/pullMessage/',
  type: "POST",
  timeout:20000,
   data: {
          forumid:idd
         }
  
});

  
  request.done(function( msg ) {
 
    unspin();
     
    msg=jQuery.parseJSON(msg);
    console.log(msg);

    if(msg.length > 0){
    
    for(i=0; i<msg.length; i++){
      
      $("#messages").append("<div class='whitepod msg'><span class='highlight'>"+msg[i].sender+'</span><br/>'+msg[i].msg+"</div>");
    }
     
    }else{
      showalert("No Messages");
    }

    showactivity();
     
  
});

request.error(function( msg ) {
  $("#loadmore").text("Loading more");
  unspin();
  console.log(msg);
  alert('check internet connection');
});

}

function sendMessage(){

  if(typeof(localStorage.profile)!="undefined"){
     profile=jQuery.parseJSON(localStorage.profile);
  }

  sender=profile[0].firstname;
  var msg=( $('input#msg').val());
  //console.log(msg);

  var request = $.ajax({
  url: API + '/forumMessage/',
  type: "POST",
  timeout:20000,
   data: {
          forumid:idd,
          msg:msg,
          sender:sender
         }
  
});

  
  request.done(function( msg ) {

    $("#messages").html("");
    $('input#msg').val("");
    $('input#msg').focus();

 
    unspin();
     
    msg=jQuery.parseJSON(msg);
    console.log(msg);

    if(msg.length > 0){
    
    for(i=0; i<msg.length; i++){
      
     $("#messages").append("<div class='whitepod msg'><span class='highlight'>"+msg[i].sender+'</span><br/>'+msg[i].msg+"</div>");
    }
     
    }else{
      showalert("No Messages");
    }

    
     
  
});

request.error(function( msg ) {
  $("#loadmore").text("Loading more");
  unspin();
  console.log(msg);
  alert('check internet connection');
});

}



function caseinit(){
   model.patientname=dbresult[idd].cropname;
   console.log(realpics);

    if(dbresult[idd].croptype=="Tuber"){
        // if(i % 2==0){
        picture='tuber.jpg';
      }else if(dbresult[idd].croptype=="Cereals"){
        picture='cereal.jpg'
      }else if(dbresult[idd].croptype=="Fruits"){
        picture='fruit.jpg'
      }else if(dbresult[idd].croptype=="Vegetables"){
        picture='vegetable.jpg'
      }else if(dbresult[idd].croptype=="Legumes"){
        picture='legume.jpg'
      }else if(dbresult[idd].croptype=="Nuts"){
        picture='nuts.jpg'
      }else{
        picture='female.png';
      }

   $(".case_status").append('<img style="height:100px;" src="images/'+picture+'"/>');

   $("#details").html("Crop Type <b class='highlight'>"+dbresult[idd].croptype+"</b><br/>Best Planting Time "+dbresult[idd].bpt+"</br>Best Harvesting Soil :<b class='highlight'>"+dbresult[idd].bps+"</b></br>Fertilizer Application :<b class='highlight'>"+dbresult[idd].wtf+"</b>"+"</br>Best Harvesting Time :<b class='highlight'>"+dbresult[idd].wtw+"</b>");
}

realpics='';

function showactivity(){
   
  getCrops("select * from crops", function(callback){
    picture=''; iconn='';

    $('.timeline').html("");


    if(dbresult.length==0){
      $(".timeline").append("<ons-list-item class='timeline-li ng-scope list__item ons-list-item-inner list__item--tappable' >No Crops Downloaded </ons-list-item>");
    }
    for(i=0; i<dbresult.length; i++){

      if(dbresult[i].croptype=="Tuber"){
        // if(i % 2==0){
        picture='tuber.jpg';
      }else if(dbresult[i].croptype=="Cereals"){
        picture='cereal.jpg'
      }else if(dbresult[i].croptype=="Fruits"){
        picture='fruit.jpg'
      }else if(dbresult[i].croptype=="Vegetables"){
        picture='vegetable.jpg'
      }else if(dbresult[i].croptype=="Legumes"){
        picture='legume.jpg'
      }else if(dbresult[i].croptype=="Nuts"){
        picture='nuts.jpg'
      }else{
        picture='female.png';
      }

      realpics=picture;

      //if(dbresult[i].status=="0"){
     if(i % 2 ==0){
      nstatus+=1;
        iconn='<i class="fa fa- fa-2x status0"></i>';
      }else{
        hstatus+=1;
        iconn='<i class="fa fa-check fa-2x status1"></i>';
      }



      $(".timeline").append('<ons-list-item onclick="casedetails('+i+')" class="timeline-li ng-scope list__item ons-list-item-inner list__item--tappable" modifier="tappable" ><ons-row class="row ons-row-inner"><ons-col width="50px" class="col ons-col-inner" style="-webkit-box-flex: 0; flex: 0 0 50px; max-width: 50px;"><img  class="timeline-image" src="images/'+picture+'"></ons-col><ons-col class="col ons-col-inner"><div class="timeline-date">'+iconn+'</div><div class="timline-from"><span class="timeline-name"><b>'+dbresult[i].cropname+'</span><span class="timeline-id">'+'  </span></div><div class="timeline-message">Crop Type: <i>'+dbresult[i].croptype+'</i></div></ons-col></ons-row></ons-list-item>');
    }
  });
}

function shopinit(){
  setTimeout(showShops,100);
}



function showPrograms(){
$("#loadmore").text("Load more");
   
  getPrograms("select * from programs", function(callback){
    picture=''; iconn='';
     
    $('.programs').html("");

    if(programs.length!=0){
     // $("#forumstatus").hide();
    }

    //$(".programs").append("<ons-row>");
    programs_html="<ons-row class='row ons-row-inner'>";
    for(i=0; i<programs.length; i++){

      picture=programs[i].programimage
      
      //$(".programs").append('<ons-col ><div class="pod menu" style="padding:0px;" onclick="programLinks("'+programs[i].id+'")"> <img src="images/progimages/t5.jpg" style="height:83px;" /><span class="highlight">Top 5</span></div></ons-col>');
      programs_html+='<ons-col class="col ons-col-inner"><div class="pod menu" style="padding:0px;" onclick="programLinks('+i+')"> <img src="'+picture+'" style="height:83px;" /><span class="highlight">'+programs[i].programname+'</span></div></ons-col>'; 
     if((i+1) % 3 ==0){
 
      programs_html+="</ons-row><ons-row class='row ons-row-inner'>";

}
     
    }

    $(".programs").append(programs_html);
  });
}





function pullCrops(){
  getCrops("select * from Crops", function(callback){
    id=0;
    for(i=0;i<dbresult.length; i++){
      id=dbresult[i].serverdb_id;
    }
    if(id==""){
      id=0;
    }

  $("#loadmore").text("Loading...");
  var request = $.ajax({
  url: API + '/getCrops/',
  type: "POST",
  timeout:20000,
   data: {
          id:id
         }
  
});

  
  request.done(function( msg ) {
    $("#loadmore").text("Load more");
    unspin();
     
    msg=jQuery.parseJSON(msg);
    console.log(msg);

    if(msg.length > 0){
    
    for(i=0; i<msg.length; i++){
      
    fasttrackdb.webdb.addCrop(msg[i].id,msg[i].cropname,msg[i].croptype,msg[i].wtf,msg[i].wtw,msg[i].bpt,msg[i].bps);
    }
     
    }else{
      showalert("Crops updated");
    }

    showactivity();
     
  
});

request.error(function( msg ) {
  $("#loadmore").text("Loading more");
  unspin();
  console.log(msg);
  alert('check internet connection');
});
});

}

function pullShops(){
  //console.log(last_serverid);

  getShop("select * from shop", function(callback){ 
    $("#loadshops").text("Loading");
    id="";
    for(i=0;i<shop.length; i++){
      id=shop[i].serverdb_id;
    }

    if(id==""){
      id=0;
    }

  $("#loadmore").text("Loading...");
 
  var request = $.ajax({
  url: API + 'getShops/',
  type: "POST",
  timeout:30000,
   data: {
          id:id
         }
  
});

  
  request.done(function( msg ) {
    $("#loadmore").text("Load more");
    unspin();
     
    msg=jQuery.parseJSON(msg);
    console.log(msg);

    if(msg.length > 0){
    
    for(i=0; i<msg.length; i++){

    fasttrackdb.webdb.addShop(msg[i].id,msg[i].shopimage,msg[i].main,msg[i].description);
    }
     
    }else{
      showalert("Shops updated");
    }

    showShops();
     
  
});

request.error(function( msg ) {
  $("#loadmore").text("Load more");
  unspin();
  console.log(msg);
  alert('check internet connection');
});
});

}

function updateProgram(){
  //console.log(programs[idd]);
  //return;



  $("#loadmore").text("Loading...");
 
  var request = $.ajax({
  url: API + 'updateProgram/',
  type: "POST",
  timeout:20000,
   data: {
          id:programs[idd].serverdb_id
         }
  
});

  
  request.done(function( msg ) {
    $("#loadmore").text("Load more");
    unspin();
     
    msg=jQuery.parseJSON(msg);
     

    if(msg.length > 0){
    
    for(i=0; i<msg.length; i++){
       

    fasttrackdb.webdb.updateProgram(msg[i].id,msg[i].programname,msg[i].video1,msg[i].video2,msg[i].video3,msg[i].video4,msg[i].video5,msg[i].programimage,msg[i].description,programs[idd].id);
    }

   // showalert("Programs updated");
     
    }else{
     // showalert("Programs updated");
    }

    showPrograms();
     
  
});

request.error(function( msg ) {
  $("#loadmore").text("Load more");
  unspin();
  console.log(msg);
  alert('check internet connection');
});


}



function pullPrograms(){
  //console.log(last_serverid);
  spin();
  $("#pullprograms").text("Loading...");
  getPrograms("select * from programs", function(callback){ 

     
    id="";

    for(i=0;i<programs.length; i++){
      id=programs[i].serverdb_id;
    }

    if(id==""){
      id=0;
    }

  $("#loadmore").text("Loading...");
 
  var request = $.ajax({
  url: API + 'getPrograms/',
  type: "POST",
  timeout:30000,
   data: {
          id:id
         }
  
});

  
  request.done(function( msg ) {
    $("#loadmore").text("Load more");
    unspin();
     
    msg=jQuery.parseJSON(msg);
    console.log(msg);

    if(msg.length > 0){
    
    for(i=0; i<msg.length; i++){

    fasttrackdb.webdb.addProgram(msg[i].id,msg[i].programname,msg[i].video1,msg[i].video2,msg[i].video3,msg[i].video4,msg[i].video5,msg[i].programimage,msg[i].description);
    }

    showalert("Programs updated");
     
    }else{
      showalert("Programs updated");
    }

    showPrograms();
     
  
});

request.error(function( msg ) {
  $("#loadmore").text("Load more");
  unspin();
  console.log(msg);
  alert('check internet connection');
});
});

}

function showShops(){
 /* 
 if(typeof(localStorage.profile)!="undefined"){
  profile=jQuery.parseJSON(localStorage.profile);
  console.log(profile);

  model.firstname=profile[0].firstname;
}
*/




  getShop("select * from shop", function(callback){
    picture=''; iconn='';

    $('.shop').html("");

    if(shop.length==0){
     $(".shop").append("<ons-list-item modifier='inset' class='timeline-li ng-scope list__item ons-list-item-inner list__item--tappable'>No Shops in this category</ons-list-item>");
    }
    for(i=0; i<shop.length; i++){

      
 
     if(i % 2 ==0){
      nstatus+=1;
        iconn='<i class="fa fa- fa-2x status0"></i>';
      }else{
        hstatus+=1;
        iconn='<i class="fa fa-check fa-2x status1"></i>';
      }

      picture=shop[i].shopimage;


      $(".shop").append('<ons-list-item onclick="shopview('+shop[i].serverdb_id+','+i+')" class="timeline-li ng-scope list__item ons-list-item-inner list__item--tappable" modifier="tappable" ><ons-row class="row ons-row-inner"><ons-col width="50px" class="col ons-col-inner" style="-webkit-box-flex: 0; flex: 0 0 50px; max-width: 50px;"><img  class="timeline-image" src="'+picture+'"></ons-col><ons-col class="col ons-col-inner"><div class="timeline-date">'+iconn+'</div><div class="timline-from"><span class="timeline-name"><b>'+shop[i].shoptype+'</b></span><span class="timeline-id">'+'  </span></div><div class="timeline-message">'+shop[i].description+'</i></div></ons-col></ons-row></ons-list-item>');
    }
  });
}


shop_id=0;

function shopview(id,index){
   main_navigator.pushPage('shopview.html');
   idd=id;
   shop_id=id;
   console.log(idd);
   //setTimeout(shopPreview,100);
    

    setTimeout(function(){

       $("#shopDetails").append('<img src="'+shop[index].shopimage+'">');
      clients=[];
      query="select * from shopclients where (shop_id = "+id+")";
      getShopClient(query, function(callback){
    
      picture=''; iconn='';
     

    //$('.shop').html("");
    console.log(clients);

    if(clients.length==0){
     // $("#forumstatus").hide();
     $(".shopview").append("<ons-list-item class='timeline-li ng-scope list__item ons-list-item-inner list__item--tappable'>No Shops in this category</ons-list-item>");
    }
    for(i=0; i<clients.length; i++){ 

      picture=clients[i].logo; 

      slogan="";

      for(j=0; j<clients[i].slogan.length; j++){
        if(j<=30){
        slogan+=clients[i].slogan[j];
      }
      }
      slogan+="...";
      
      $(".shopview").append('<ons-list-item onclick="clientDetails('+i+')" class="timeline-li ng-scope list__item ons-list-item-inner list__item--tappable" modifier="tappable" ><ons-row class="row ons-row-inner"><ons-col width="50px" class="col ons-col-inner" style="-webkit-box-flex: 0; flex: 0 0 50px; max-width: 50px;"><img  class="timeline-image" src="'+picture+'"></ons-col><ons-col class="col ons-col-inner"><div class="timeline-date">'+iconn+'</div><div class="timline-from"><span class="timeline-name"><b>'+clients[i].companyname+'</b></span><span class="timeline-id">'+'  </span></div><div class="timeline-message">'+slogan+'</i></div></ons-col></ons-row></ons-list-item>');
    }
  });
   },100);



}

 


function clientDetails(id){
  main_navigator.pushPage('clientdetails.html');
 
 /* if(typeof(localStorage.profile)!="undefined"){

  profile=jQuery.parseJSON(localStorage.profile);
  console.log(profile);
  console.log(profile[0].firstname);

  model.firstname=profile[0].firstname;
} */

 setTimeout(function(){

  if(clients.length!=0){ 

    picture=clients[id].logo;

     $(".clientsView").append("<img  src='"+picture+"'/><div class='highlight'>"+clients[id].companyname+"</div><br/><i>"+clients[id].slogan+"</i>");

      $(".details").append("<div class='podhead'>Description</div><div class='contentpod'>"+clients[id].description+"</div><div class='podhead'>Email</div><div class='contentpod'>"+clients[id].email+"</div><div class='podhead'>Phone Number</div><div class='contentpod'>"+clients[id].phoneno+"</div><div class='podhead'>Website</div><div class='contentpod'>"+clients[id].website+"</div>");

       
    }
     
   },100);

  idd=id;
  //setTimeout(clientsInfo,100);
}
function updateShop(){
  console.log(shop_id);

     

  $("#loadmore").text("Loading...");
 
  var request = $.ajax({
  url: API + 'updateShop/',
  type: "POST",
  timeout:20000,
   data: {
          shop_id:shop_id
         }
  
});

  request.done(function( msg ) {
    $("#loadmore").text("Load more");
    unspin();
     
    msg=jQuery.parseJSON(msg);
    console.log(msg);



    if(msg.length > 0){
    
    for(i=0; i<msg.length; i++){

       

    fasttrackdb.webdb.updateShop(msg[i].id,msg[i].shopimage,msg[i].main,msg[i].description,shop_id);
    }
     
    }else{
      showalert("No new shops");
    }

      showalert("Shops updated");

   // main_navigator.pushPage("shops.html");
    
     
  
});

request.error(function( msg ) {
  $("#loadmore").text("Load more");
  unspin();
  console.log(msg);
  alert('check internet connection');
});

}

 
function updateClient(){
  //console.log(last_serverid);
  id="";
   console.log(clients[idd]);

    //console.log(clients);

    if(id==""){
      id=0;
    }

    console.log(shop_id);
    console.log(id);

  $("#loadmore").text("Loading...");
 
  var request = $.ajax({
  url: API + 'updateClient/',
  type: "POST",
  timeout:20000,
   data: {
          id:clients[idd].serverdb_id
         }
  
});

  request.done(function( msg ) {
    $("#loadmore").text("Load more");
    unspin();
     
    msg=jQuery.parseJSON(msg);
    console.log(msg);



    if(msg.length > 0){
    
    for(i=0; i<msg.length; i++){

    fasttrackdb.webdb.updateShopClients(msg[i].id,msg[i].shop_id,msg[i].companyname,msg[i].slogan,msg[i].description,msg[i].logo,msg[i].email,msg[i].phoneno,msg[i].website,clients[idd].id);
    }
     
    }else{
      showalert("No new shops");
    }

      showalert("Shops updated");

   // main_navigator.pushPage("shops.html");
    
     
  
});

request.error(function( msg ) {
  $("#loadmore").text("Load more");
  unspin();
  console.log(msg);
  alert('check internet connection');
});

}




function pullClients(){
  //console.log(last_serverid);
  id="";
  getShopClient("select * from shopclients", function(callback){ 

    for(i=0;i<clients.length; i++){
      id=clients[i].serverdb_id;
    }

    //console.log(clients);

    if(id==""){
      id=0;
    }

    console.log(shop_id);
    console.log(id);

  $("#loadmore").text("Loading...");
 
  var request = $.ajax({
  url: API + 'getShopClient/',
  type: "POST",
  timeout:20000,
   data: {
          id:id 
         }
  
});

  request.done(function( msg ) {
    $("#loadmore").text("Load more");
    unspin();
     
    msg=jQuery.parseJSON(msg);
    console.log(msg);

    if(msg.length > 0){
    
    for(i=0; i<msg.length; i++){

    fasttrackdb.webdb.addShopClients(msg[i].id,msg[i].shop_id,msg[i].companyname,msg[i].slogan,msg[i].description,msg[i].logo,msg[i].email,msg[i].phoneno,msg[i].website);
    }
     
    }else{
      showalert("No new shops");
    }

      showalert("Shops updated");

   // main_navigator.pushPage("shops.html");
    
     
  
});

request.error(function( msg ) {
  $("#loadmore").text("Load more");
  unspin();
  console.log(msg);
  alert('check internet connection');
});
});

}

function start(){
  main_navigator.pushPage('link.html');
  //setTimeout(showactivity,100);
  setTimeout(showPrograms,100);
}

function showAbout(){
  main_navigator.pushPage('about.html');
   
}


function logininit(){
 main_navigator.pushPage('link.html');
 return;

   if(typeof(localStorage.profile) !="undefined"){
     main_navigator.pushPage('link.html');
     setTimeout(showPrograms,100);
   
   }else{
    setTimeout(showPrograms,100);
    //setTimeout(showPrograms,100);
    // main_navigator.pushPage('login.html');
   }

   return;

console.log(typeof(localStorage.usertype));
  if(typeof(localStorage.usertype) !="undefined"){
    if(localStorage.usertype=="user"){
      main_navigator.pushPage('user.html');
    }else{
      if(typeof(localStorage.profile) !="undefined"){
        main_navigator.pushPage('link.html');
        setTimeout(showactivity,200);
      }else{
        main_navigator.pushPage('login.html');
      }
    }
  }else{
    main_navigator.pushPage('login.html');
  }
}

function login()
{

  spin();

var pin =  ( $('input#login_pin').val());
 
    
    if (pin == "") {
      showalert("Please Enter your PIN Number.");
      $('input#login_pin').focus(); 
      unspin();
      return
    }
  
  var phone = $('input#login_phone').val();
  //phone='+2347033556109';
  
   if (phone == "") {
      showalert("Please Enter your Phone Number");
      $('input#login_phone').focus(); 
        unspin();
      return
    }


  var hashpass = CryptoJS.MD5(pin);
  hashpass = hashpass.toString();

   if(typeof(localStorage.profile) !="undefined"){
      profile=jQuery.parseJSON(localStorage.profile);

      if(phone==profile[0].phone){
          main_navigator.pushPage('link.html');
          setTimeout(showactivity,300);
        return; 
      }
   
  }
  
  console.log(phone);  

   var request = $.ajax({
  url:   API+ 'verifySubscriber',
  type: "POST",
   data: {
          
         phone: phone, 
         password: hashpass, 
         }
  
});

  
request.done(function( msg ) {
unspin();
   

nmsg=jQuery.parseJSON(msg);
console.log(msg); 


if(nmsg.length > 0){ 

  localStorage.setItem("profile",msg);
  main_navigator.pushPage('link.html');
  setTimeout(showPrograms,100);

}else{
  showalert("Invalid Pin or Phonenumber");
} 

});

request.error(function( msg ) {
  unspin();
  console.log(msg); 
  alert('check internet connection');
});
            
  
}

function reload(){
   main_navigator.pushPage('start.html',{ animation: "none" });
}

function onPositionUpdate(position)
            {
                var lat = position.coords.latitude;
                var lng = position.coords.longitude;
                //alert("Current position: " + lat + " " + lng);
            }


function init()
{

 

if(navigator.geolocation)
                navigator.geolocation.getCurrentPosition(onPositionUpdate);
            else
               console.log("navigator.geolocation is not available");



  fasttrackdb.webdb.open();
  fasttrackdb.webdb.createTablePosts();
  fasttrackdb.webdb.createTableShop();
  fasttrackdb.webdb.createTableShopClients();
  fasttrackdb.webdb.createTablePrograms();
  fasttrackdb.webdb.createTableEvents();
   
 
 
  main_navigator.pushPage('start.html',{ animation: "none" });

   

  var activated = localStorage.getItem("capture_pin");
  if (activated == null)
  {
        main_navigator.pushPage('start.html',{ animation: "none" });
  }
  else
  {

     var cardidarray = localStorage.getItem("capture_card_ids");


     if (cardidarray == null || cardidarray == 'null')
      {
                      
       main_navigator.pushPage('link.html',{ animation: "none" });
                      
       }
       else
       {
       
       


        main_navigator.pushPage('pin.html',{ animation: "none" });
       }
  }


//getDeviceHandler();
main_navigator.on("postpush", function() {


  // Add a handler for prepush event

if ((main_navigator.getCurrentPage().page) == 'programs.html') {
  showPrograms();

}

else if ((main_navigator.getCurrentPage().page) == 'pay.html')
{
    processPayCode();
}

else if ((main_navigator.getCurrentPage().page) == 'pin.html')
{
   window.plugins.touchid.isAvailable(
      function(msg) 
      {
         window.plugins.touchid.verifyFingerprint(
          'Scan your fingerprint please', // this will be shown in the native scanner popup
           function(msg) {
              
              main_navigator.pushPage('main.html');

           }, // success handler: fingerprint accepted
           function(msg) {
          navigator.app.exitApp();

           } // error handler with errorcode and localised reason
        );
              
      },    // success handler: TouchID available
      function(msg) {} // error handler: no TouchID available
);

}

else if ((main_navigator.getCurrentPage().page) == 'signup.html')
{
 
 
 // loadjscssfile("js/bootstrap-formhelpers.min.js", "js");
}
else if (((main_navigator.getCurrentPage().page) == 'transactions.html') || (main_navigator.getCurrentPage().page) == 'main.html') 
{

 var height = $(window).height();
 height = height - 180;

 $("#holder").css("margin-top",height);
 $("#holder").show();
 console.log('show me');
 console.log(height);
setTimeout(getTransactions,1000);
  
  
}



}); 



}

 
 

 var onCardIOComplete = function(response) {
        
        $('#card_number').val(response.card_number)      
        $('#card_mm').val(response.expiry_month)      
        $('#card_yy').val(response.expiry_year)      
        $('#card_cvv').val(response.cvv)   

        $('#card_number').trigger( "change" );   
  
      };
  
      var onCardIOCancel = function(response) {
      //  showalert ("test")
      //  alert(JSON.stringify(response));
          showalert("card.io scan cancelled");
      };


function cardReader() {
              CardIO.scan({
                  "collect_expiry": true,
                  "collect_cvv": false,
                  "collect_zip": false,
                  "shows_first_use_alert": true,
                  "disable_manual_entry_buttons": false
                },
                onCardIOComplete,
                onCardIOCancel
              );
            }
      




 

function showalert(message)
{
    try
    {
      // toast.showLong(message);
      window.plugins.toast.showShortCenter(message)
    }
    catch(err)
    {
      alert (message);
    }
}


function validateEmail($email) {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  if( !emailReg.test( $email ) ) {
    return false;
  } else {
    return true;
  }
}


function verifyPayment(){
  return true;
}

function generateCode()
{
        
        //    var deviceVersion = device.version;
        //   var deviceModel = device.model;
        //   var deviceUuid = device.uuid;
            var deviceId = Math.floor(Math.random()*100000000) ;
            //p for Phone
            var tosend =  deviceId;
            return tosend;
}


function getDeviceHandler()
{
               var tosend = localStorage.getItem("capture_send_code");
              
              if (tosend == null)
              {
                tosend = generateCode();
                localStorage.setItem("capture_send_code", tosend);  
              }
              
}




function spin()
{
  $('.nospin').hide();
   $('.spinner').show();
   $('.button--large').attr('disabled','disabled');
}

function unspin()
{
  $('.nospin').show();
   $('.spinner').hide();
 
   $('.button--large').removeAttr('disabled');
}


 


jQuery(function($) {
  
 
    
        
        try
        {
          Pwc.setPublishableKey('uFmz/uE/SDT6GupOrSEXIZXGByjQ0zFkPyc9LqKHFqnTI0WPN3JS5kQPo/j9or0TOXlqMQj2lzHn/UGsQT4XeQ==');
        }
          
      
        catch(e)
        {
          
        }
        
        
         
 
         
                
                
 });



 


