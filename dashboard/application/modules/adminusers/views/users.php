 
<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="content-type" content="text/html" />
	<meta name="author" content="Joshua Ajayi" />
    <!-- Latest compiled and minified CSS -->
    
     <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/bootstrap-cerulean.css') ?>">
 <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/bootstrap-responsive.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/charisma-app.css') ?>">
    
    <!-- Optional theme -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">


	<title>Channe M1 mobile app</title>
</head>

<body>
  <div class="container-fluid">
        <div class="row-fluid">
                <div class="alerts">
                </div>
        

            <div class="row-fluid">
                <div class="span12 center login-header">
                    <h2>Channel M1 Mobile App </h2>
                </div><!--/span-->
            </div><!--/row-->
            
            <div class="row-fluid">
                <div class="well span5 center login-box">
                     
                    <?php if(isset($errormsg)){
                        echo '<div class="alert alert-danger">Invalid Username and Password. </div>';
                    }else{
                          echo '<div class="alert alert-info">Please login with your Username and Password. </div>';
                    }
                    ?>
                        
                    <form class="form-horizontal" action="<?php echo base_url('adminusers/login'); ?>" method="post">
                        <fieldset>
                        <div class="input-prepend" data-rel="tooltip" data-original-title="Username">
                                <span class="add-on"><i class="icon-user"></i></span>
                                <select name="usertype">
                                    <option>Admin</option>
                                     <option>User</option>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                            <div class="input-prepend" data-rel="tooltip" data-original-title="Username">
                                <span class="add-on"><i class="icon-user"></i></span><input autofocus="" class="input-large span10" name="username" id="username" type="text" autocomplete="off">
                            </div>
                            <div class="clearfix"></div>

                            <div class="input-prepend" data-rel="tooltip" data-original-title="Password">
                                <span class="add-on"><i class="icon-lock"></i></span><input class="input-large span10" name="password" id="password" type="password" autocomplete="off">
                            </div>
                            <div class="clearfix"></div>

                            <p class="center span5">
                            <input type="submit" class="btn btn-primary" value="login"/> 
                            </p>
                        </fieldset>
                    </form>
                    
                </div><!--/span-->
            </div><!--/row-->                       </div><!--/fluid-row-->
        
    </div>
  
</div>
<!-- Latest compiled and minified JavaScript -->
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
</body>
</html>