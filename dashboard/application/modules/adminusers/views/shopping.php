    <?php 
    $sesiondata=$this->session->all_userdata();

     if(! isset($sesiondata['username'])){
              redirect(base_url('adminusers/logout'));
  }
   
   ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Channel M1 mobile app</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
     
    <meta name="author" content="Joshua Ajayi">

    <!-- The styles -->
     
    <style type="text/css">
      body {
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }
    </style>
     
     <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/bootstrap-cerulean.css'); ?>">
      <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/charisma-app.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/bootstrap-responsive.css'); ?>">
      <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/jquery-ui-1.8.21.custom.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/chosen.css'); ?>">

        <link rel="stylesheet" href="<?php echo base_url('assets/admin/css//uniform.default.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/admin/css//colorbox.css'); ?>">
      <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/jquery.cleditor.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/jquery.noty.css'); ?>">
 
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/noty_theme_default.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/elfinder.min.css'); ?>">
      <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/elfinder.theme.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/opa-icons.css'); ?>">
       <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/style.css'); ?>">
     

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
     
    
    <!-- jQuery -->
    <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
     
</head>

<body>
     
    <!-- topbar starts -->
     <div class="navbar">
        <div class="navbar-inner">
            <div class="container-fluid">
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <a class="brand" href="#">Channel M1 Mobile App</a>
                
             
                
                <!-- user dropdown starts -->
                
                <div class="btn-group pull-right">
  
                    <div class="btn-group pull-right">
                    <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="icon-user"></i> <span> <?php echo $sesiondata['username']?></span><span class="hidden-phone"> </span>
                        <span class="caret"></span>

                    </a>
                    <ul class="dropdown-menu">

                        <li><a href="<?php echo base_url('adminusers/logout'); ?>">Logout</a></li>
                    </ul>
                </div>
                    <ul class="dropdown-menu">

                        <li><a href="<?php echo base_url('adminusers'); ?>">Logout</a></li>
                    </ul>

                </div>
                 
                <!-- user dropdown ends -->
                
                <div class="top-nav nav-collapse">
                     
                </div><!--/.nav-collapse -->
            </div>
        </div>
    </div>
  

    <div class="container-fluid">
        <div class="row-fluid">
                
            <!-- left menu starts -->
          <div class="span2 main-menu-span">
                <div class="well nav-collapse sidebar-nav">
                    <ul class="nav nav-tabs nav-stacked main-menu">
                        <li class="nav-header hidden-tablet">Admin Menu</li>

                        <li id="ld" style="margin-left: -2px;"><a class="ajax-link" href="<?php echo base_url('post/showposts/'); ?>"><i class="icon-upload"></i><span class="hidden-tablet">Trends</span></a></li>
                        
                       <li id="ld" style="margin-left: -2px;"><a class="ajax-link" href="<?php echo base_url('post/showevents/'); ?>"><i class="icon-upload"></i><span class="hidden-tablet">Events</span></a></li>
                        <li  style="margin-left: -2px;" class="active"><a class="ajax-link disabled" href="#"><i class="icon-eye-open"></i><span class="hidden-tablet">Shopping</span></a></li>
                        <li id="ld"  style="margin-left: -2px;"><a class="ajax-link" href="<?php echo base_url('adminusers/programview/'); ?>"><i class="icon-user"></i><span class="hidden-tablet">Programs</span></a></li>
                        <li  style="margin-left: -2px;"><a class="ajax-link disabled" href="<?php echo base_url('adminusers/othersview/'); ?>"><i class="icon-eye-open"></i><span class="hidden-tablet">Others</span></a></li>
 
                </ul></div><!--/.well -->
            </div><!--/span-->
            <!-- left menu ends -->
             
             
            <div id="content" class="span10">
            <!-- content starts -->
                    <div class="alerts">
                </div>
        
<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home/</a>  
        </li>
        <li>
            <a href="#">Adminusers</a>
        </li>

    </ul>
</div>

<div class="row-fluid sortable ui-sortable">        
     
    <div class="box span12">
                    <div class="box-header well" data-original-title="" >
                        <h2><i class="icon-user"></i> Members</h2>
                     
                            <div class="pull-right">
                             <?php
                             if($sesiondata['usertype']=="Superadmin"){
                                echo '<a href="#" data-target=".bs" class="btn btn-primary " data-toggle="modal" style="" >Header <i class="icon-white icon-plus"></i></a>';
                             }
                             ?>
                            
                        </div>
                         
                    </div>
                    <div class="box-content">
                        <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid">
                        <table class="table table-striped table-bordered bootstrap-datatable datatable dataTable" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info" >
                          <thead>
                              <tr role="row">
                              <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" width="20px">S/N</th>
                              <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" >Shop Image</th>
                              <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"   >Shop Name</th>
                              
                              <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" >Description</th>
                             
                                               
                              <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"  >Action</th>
                              
                          </tr>
                          </thead>   
                          
                      <tbody role="alert" aria-live="polite" aria-relevant="all"> 
                      <?php 

                      $c=0;
                        foreach ($query->result() as $value) {
                            $delete_url =  base_url('adminusers/delete/') .'/'. $value->id; 
                             $edit_url =  base_url('adminusers/edit/') .'/'. $value->id; 
                             if($sesiondata['usertype'] == "Admin"){
                                $target='';
                                $delete_url='#';
                             }else{
                                $target=".bs";
                             }
                            $c+=1;
                            if ($c%2==0){
                                echo '<tr class="even">';
                            }else{
                                echo '<tr class="odd">';
                            }
                            echo '<td class="sorting_1">'.$c.'</td>
                            <td class="sorting_1">'.$value->main.'</td>
                            <td class="sorting_1" style=""><div class="dv">
                            <img src="'.base_url('shop/'.$value->shopimage).'" style="width:50px; height:50px;"/> </div></td>
                              
                                <td>'.$value->description.'</td>
                                <td class="sorting_1">
                                     
                                    <a  class="btn btn-info" href="'.'" data-target="'.$target.$value->id.'" data-toggle="modal"   >
                                        <i class="icon-edit icon-white"></i> 
                                        Edit</a>

                                         <a  class="btn btn-warning" href="'.base_url("adminusers/shopclients/".$value->id).'"    >
                                        <i class="icon-edit icon-white"></i> 
                                        View Clients</a>

                                        <a  class="btn btn-danger" href="'.base_url("adminusers/shopdelete/".$value->id).'"    >
                                        <i class="icon-trash icon-white"></i> 
                                        </a>
                                </td>
                            </tr>';
                        }

                      ?>
                      
                        </tbody>
                </table>
        </div> 



                   
                        </div>
                </div>




</div>
                            <!-- content ends -->
            </div><!--/#content.span10-->
                </div><!--/fluid-row-->
        <hr>

        <div class="modal fade bs" id="myModal">
            <div class="modal-header ">
                <button type="button" class="close" data-dismiss="modal">X</button>
                <h3>Add New Shop</h3>
            </div>

           <div class="modal-body">
          <?php echo form_open_multipart('adminusers/createshop/') ;?>
                 <label>Shop Name</label>
                 <input class="form-control" name="main" type="text">

                 <label>Description</label>
                 <input class="form-control" name="description" type="text">
                 
                  <input type="file" class="form-control" name="userfile"/>
                   
                    <div class="modal-footer">
                <a href="#" class="btn" data-dismiss="modal">Close</a>
                <input value="create user" type="submit" class="btn btn-primary"/>
            </div>
          </form>
       </div>
</div>
        <?php 
            foreach ($query->result() as $value) {

                 
                 
                echo ' <div class="modal fade bs'.$value->id.'" id="myModal">
            <div class="modal-header ">
                <button type="button" class="close" data-dismiss="modal">X</button>
                <h3>Update Shop</h3>
            </div>

                <div class="modal-body">
         '.form_open_multipart('adminusers/shopedit/'.$value->id).' 
             <div class="formdiv">

             <img src="'.base_url('shop'.$value->shopimage).'" />

              <label>Shop Name</label>
                 <input class="form-control" name="main" value="'.$value->main.'" type="text">

                 <label>Description</label>
                 <input class="form-control" name="description" value="'.$value->description.'" type="text">
                 
                  
                  <input type="file" name="userfile" class="form-control"/>
                    
                   
                    </div>
                    <div class="modal-footer">
                         
                        <input value="Update Shop" type="submit" class="btn btn-primary"/>
                    </div>
            
          </form>
       </div></div>';


            }
        ?>
 

         
        




        <footer>
            <p class="pull-left">© <a href="http://www.designdistrictonline.com" target="_blank">Design District</a> 2014</p>
        </footer>
        
    </div>






    <!-- jQuery -->
    <script src="<?php echo base_url('assets/admin/js/jquery-1.7.2.min.js');?>"></script>
    <script>
    bool=true;
    $(".pass").click(function(){
        if (bool){$(".formdiv").append('<label>Password</label><input type="password" name="password" placeholder="new password"/>') ; 
        bool=false}
    });
    </script>
    <!-- jQuery UI -->
    <script src="<?php echo base_url('assets/admin/js/jquery-ui-1.8.21.custom.min.js');?>"></script>
    <!-- transition / effect library -->
    <script src="<?php echo base_url('assets/admin/js/bootstrap-transition.js');?>"></script>
    <!-- alert enhancer library -->
    <script src="<?php echo base_url('assets/admin/js/bootstrap-alert.js');?>"></script>
    <!-- modal / dialog library -->
    <script src="<?php echo base_url('assets/admin/js/bootstrap-modal.js');?>"></script>
    <!-- custom dropdown library -->
    <script src="<?php echo base_url('assets/admin/js/bootstrap-dropdown.js');?>"></script>
    <!-- scrolspy library -->
    <script src="<?php echo base_url('assets/admin/js/bootstrap-scrollspy.js');?>"></script>
    <!-- library for creating tabs -->
    <script src="<?php echo base_url('assets/admin/js/bootstrap-tab.js');?>"></script>
    <!-- library for advanced tooltip -->
    <script src="<?php echo base_url('assets/admin/js/bootstrap-tooltip.js');?>"></script>
    <!-- popover effect library -->
    <script src="<?php echo base_url('assets/admin/js/bootstrap-popover.js');?>"></script>
    <!-- button enhancer library -->
    <script src="<?php echo base_url('assets/admin/js/bootstrap-button.js');?>"></script>
    <!-- accordion library (optional, not used in demo) -->
    <script src="<?php echo base_url('assets/admin/js/bootstrap-collapse.js');?>"></script>
    <!-- carousel slideshow library (optional, not used in demo) -->
    <script src="<?php echo base_url('assets/admin/js/bootstrap-carousel.js');?>"></script>
    <!-- autocomplete library -->
    <script src="<?php echo base_url('assets/admin/js/bootstrap-typeahead.js');?>"></script>
    <!-- tour library -->
    <script src="<?php echo base_url('assets/admin/js/bootstrap-tour.js');?>"></script>
    <!-- library for cookie management -->
    <script src="<?php echo base_url('assets/admin/js/jquery.cookie.js');?>"></script>
    <!-- calander plugin -->
    <script src="<?php echo base_url('assets/admin/js/fullcalendar.min.js'); ?>"></script>
    <!-- data table plugin -->
    <script src="<?php echo base_url('assets/admin/js/jquery.dataTables.min.js');?>"></script>

    <!-- chart libraries start -->
    <script src="<?php echo base_url('assets/admin/js/excanvas.js');?>"></script>
    <script src="<?php echo base_url('assets/admin/js/jquery.flot.min.js');?>"></script>
    <script src="<?php echo base_url('assets/admin/js/jquery.flot.pie.min.js');?>"></script>
    <script src="<?php echo base_url('assets/admin/js/jquery.flot.stack.js');?>"></script>
    <script src="<?php echo base_url('assets/admin/js/jquery.flot.resize.min.js');?>"></script>
    <!-- chart libraries end -->

    <!-- select or dropdown enhancer -->
    <script src="<?php echo base_url('assets/admin/js/jquery.chosen.min.js');?>"></script>
    <!-- checkbox, radio, and file input styler -->
    <script src="<?php echo base_url('assets/admin/js/jquery.uniform.min.js');?>"></script>
    <!-- plugin for gallery image view -->
    <script src="<?php echo base_url('assets/admin/js/jquery.colorbox.min.js');?>"></script>
    <!-- rich text editor library -->
    <script src="<?php echo base_url('assets/admin/js/jquery.cleditor.min.js');?>"></script>
    <!-- notification plugin -->
    <script src="<?php echo base_url('assets/admin/js/jquery.noty.js');?>"></script>
    <!-- file manager library -->
    <script src="<?php echo base_url('assets/admin/js/jquery.elfinder.min.js');?>"></script>
    <!-- star rating plugin -->
    <script src="<?php echo base_url('assets/admin/js/jquery.raty.min.js');?>"></script>
    <!-- for iOS style toggle switch -->
    <script src="<?php echo base_url('assets/admin/js/jquery.iphone.toggle.js');?>"></script>
    <!-- autogrowing textarea plugin -->
    <script src="<?php echo base_url('assets/admin/js/jquery.autogrow-textarea.js');?>"></script>
    <!-- multiple file upload plugin -->
    <script src="<?php echo base_url('assets/admin/js/jquery.uploadify-3.1.min.js');?>"></script>
    <!-- history.js for cross-browser state change on ajax -->
    <script src="<?php echo base_url('assets/admin/js/jquery.history.js');?>"></script>
    <!-- application script for Charisma demo -->
    <script src="<?php echo base_url('assets/admin/js/charisma.js');?>"></script>
    
    
    </body>
    </html>