<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class adminusers extends MX_Controller {

 function index()
	{
     
	$this->load->model('mdl_adminusers');
   
  $this->load->library('session');
  if ($this->session->userdata('username')){
    $this->mdl_adminusers->tablename="programs";
     $query=$this->mdl_adminusers->get('programname');
     $data['query']=$query;

     if ($this->session->userdata('usertype') == "Admin"){
          $this->track();
        } else{
           $this->load->view('adminusers',$data);}
  }else{
  $this->load->view('users');}
    }


    function getPosts(){
  $data=$this->get_form_data();
  //$id=1;
   
  $this->load->model('mdl_adminusers');
  $this->mdl_adminusers->tablename='posts';
  $query=$this->mdl_adminusers->_custom_query("select * from posts where id > ".$id);


  foreach ($query->result() as $value) {
         $path = base_url('Postpics/'.$value->picname);
         $type = pathinfo($path, PATHINFO_EXTENSION);
         $data = file_get_contents($path);
         $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
         $value->picname=$base64;
         //print_r($base64);
          
       }
   
  print_r(json_encode($query->result()));
 
}

function getEvents(){
  $data=$this->get_form_data();
  $id=$data['id'];
   
  $this->load->model('mdl_adminusers');
  $this->mdl_adminusers->tablename='event';
  $query=$this->mdl_adminusers->_custom_query("select * from event where id > ".$id);


  foreach ($query->result() as $value) {
         $path = base_url('Eventpics/'.$value->picname);
         $type = pathinfo($path, PATHINFO_EXTENSION);
         $data = file_get_contents($path);
         $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
         $value->picname=$base64;
       }
   
  print_r(json_encode($query->result()));
 
}


    function registerSubscriber(){
      $this->load->model('mdl_adminusers');
      $this->mdl_adminusers->tablename="subscribers";
      $data=$this->get_form_data();
      $data['password']=md5($data['password']);
      $this->mdl_adminusers->_insert($data);
    }

    function getShops(){
       $data=$this->get_form_data();
       $id=$data['id'];
   
       $this->load->model('mdl_adminusers');
       $this->mdl_adminusers->tablename='shop';
       $query=$this->mdl_adminusers->_custom_query("select * from shop where id > ".$id);

       foreach ($query->result() as $value) {
         $path = base_url('shop/'.$value->shopimage);
         $type = pathinfo($path, PATHINFO_EXTENSION);
         $data = file_get_contents($path);
         $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
         $value->shopimage=$base64;
       }
   
       print_r(json_encode($query->result()));

    }

     function getPrograms(){
       $data=$this->get_form_data();
       $id=$data['id'];

       $id=1;
   
       $this->load->model('mdl_adminusers');
       $this->mdl_adminusers->tablename='shop';
       $query=$this->mdl_adminusers->_custom_query("select * from programs where id > ".$id);

       foreach ($query->result() as $value) {
         $path = base_url('programimage/'.$value->programimage);
         $type = pathinfo($path, PATHINFO_EXTENSION);
         $data = file_get_contents($path);
         $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
         $value->programimage=$base64;
       }
   
       print_r(json_encode($query->result()));

    }

    function updateProgram(){
       $data=$this->get_form_data();
       $id=$data['id'];
   
       $this->load->model('mdl_adminusers');
       $this->mdl_adminusers->tablename="programs";
       $query=$this->mdl_adminusers->get_where($id);
       print_r(json_encode($query->result()));

    }

    function updateShop(){
     $data=$this->get_form_data();
       $id=$data['shop_id'];
   
       $this->load->model('mdl_adminusers');
       $this->mdl_adminusers->tablename="shop";
       $query=$this->mdl_adminusers->get_where($id);

        foreach ($query->result() as  $value) {
 
          

         $path = base_url('shop/'.$value->shopimage);
         $type = pathinfo($path, PATHINFO_EXTENSION);
         $data = file_get_contents($path);
         $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
         $value->shopimage=$base64;

       }
 
       print_r(json_encode($query->result()));
}

    function updateClient(){
       $data=$this->get_form_data();
       $id=$data['id'];
   
       $this->load->model('mdl_adminusers');
       $this->mdl_adminusers->tablename="shopclients";
       $query=$this->mdl_adminusers->get_where($id);

       $shopimage="";
       $shopdescription="";

        foreach ($query->result() as  $value) {

          $this->mdl_adminusers->tablename="shop";
          $shops=$this->mdl_adminusers->get_where($value->shop_id);

          foreach ($shops->result() as  $shopvalue) {
            $value->shopimage=$shopvalue->shopimage;
            $value->shopdescription=$shopvalue->description;
          }

          

         $path = base_url('shopimages/'.$value->logo);
         $type = pathinfo($path, PATHINFO_EXTENSION);
         $data = file_get_contents($path);
         $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
         $value->logo=$base64;

       }



       print_r(json_encode($query->result()));

    }

     function getShopClient(){
       $data=$this->get_form_data();
       $id=$data['id'];
   
       $this->load->model('mdl_adminusers');
       $this->mdl_adminusers->tablename='shop';
       $query=$this->mdl_adminusers->_custom_query("select * from shopclients where id > ".$id) ;
   
      foreach ($query->result() as $value) {
         $path = base_url('shopimages/'.$value->logo);
         $type = pathinfo($path, PATHINFO_EXTENSION);
         $data = file_get_contents($path);
         $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
         $value->logo=$base64;
       }

       print_r(json_encode($query->result()));

    }



    function verifySubscriber(){
      $this->load->model('mdl_adminusers');
      $this->mdl_adminusers->tablename="subscribers";
      $data=$this->get_form_data();

      $this->db->where('phone', $data['phone']);
      $this->db->where('password', md5($data['password']));
      $query=$this->db->get('subscribers');
      print_r(json_encode($query->result()));
    }

    function programlinks(){
      $this->load->model('mdl_adminusers');
      $this->mdl_adminusers->tablename="programs";
      $query=$this->mdl_adminusers->get_where(1);
      print_r(json_encode($query->result()));

    }

    function edit(){
      $this->load->model('mdl_adminusers');
       $this->load->library('session');
        $this->mdl_adminusers->tablename="programs";
      $id = $this->uri->segment(3);
      $data=$this->get_form_data();
     $query = $this->mdl_adminusers->get_where($id);

      
     if($data['uploadtype']=="Image"){
      $config['upload_path'] = './programimage/';
    }else{
      $config['upload_path'] = './programvideos/';
    }
     
    $config['allowed_types'] = '*';
    //$config['file_name'] = $id.'pics';
    $config['overwrite']=true;
     
    $this->load->library('upload', $config);
    



    if ( !($this->upload->do_upload()))
    {
    print_r($this->upload->display_errors());
     $this->mdl_adminusers->_update($id,$data);
    }else{
          foreach ($this->upload->data() as $key=>$value) {
            if($key=="orig_name"){
               if($data['uploadtype']=="Image"){
              $data['programimage']=$value;
            }elseif ($data['uploadtype']=="Video 1") {
               $data['video1']=$value;
            }elseif ($data['uploadtype']=="Video 2") {
               $data['video2']=$value;
            }elseif ($data['uploadtype']=="Video 3") {
               $data['video3']=$value;
            }elseif ($data['uploadtype']=="Video 4") {
               $data['video4']=$value;
            }elseif ($data['uploadtype']=="Video 5") {
               $data['video5']=$value;
            }
          }
        }
         $this->mdl_adminusers->_update($id,$data);

        redirect(base_url('adminusers/programview'));
    die(); 
         
          
    }

die();

    
     $config=[];

     $this->load->library('image_lib');

     $config['image_library'] = 'gd2';
     $config['source_image'] = './programimage/'.$data['programimage'];
     $config['new_image'] = './programimage/thumbnail/'.$data['programimage'];
     $config['create_thumb'] = TRUE;
     $config['maintain_ratio'] = TRUE;
     $config['width']  = 75;
     $config['height'] = 50;

     $this->load->library('image_lib', $config); 

     $this->image_lib->resize();
    echo $this->image_lib->display_errors();
 

    
    }

    function eventview(){
       $this->load->library('session');
      $this->load->model('mdl_adminusers');
      $this->mdl_adminusers->tablename="event";
       $query = $this->mdl_adminusers->get('id');
       $data['query']=$query;
      $this->load->view('event',$data);
    }
     function createevent(){
        $this->load->model('mdl_adminusers');
      $this->load->library('session');
      $this->mdl_adminusers->tablename="event";
      $data= $this->get_form_data();
      
      $this->mdl_adminusers->_insert($data);
      $query=$this->mdl_adminusers->get('id');
      $mydata['query']=$query;
      $this->load->view('event',$mydata);
    }

    function eventedit(){
      $this->load->model('mdl_adminusers');
       $this->load->library('session');
        $this->mdl_adminusers->tablename="event";
      $id = $this->uri->segment(3);
      $data=$this->get_form_data();

    $config['upload_path'] = './events/';
    $config['allowed_types'] = 'jpg';
    $config['file_name'] = $id.'pics';
    $config['overwrite']=true;
     
    $this->load->library('upload', $config);
    
    if ( ! $this->upload->do_upload())
    {
    echo 'error uploading file';
    }else{
    
        $img_data=$this->upload->data();
            $new_imgname=$id.'.jpg';
            $new_imgpath=$img_data['file_path'].$new_imgname;
            rename($img_data['full_path'], $new_imgpath);
            
            //$data['picture']=$id.'pics.jpg';
    }



     $query = $this->mdl_adminusers->get_where($id);

      
     $this->mdl_adminusers->_update($id,$data);
     $query=$this->mdl_adminusers->get('id');
     $mydata['query']=$query;
      
     $this->load->view('event',$mydata);
    }

    function othersview(){
       $this->load->library('session');
       $this->load->view('others');
    }

    function othersedit(){
      $config['upload_path'] = './others/';
    $config['allowed_types'] = 'jpg';
    $config['file_name'] = 'others';
    $config['overwrite']=true;
     
    $this->load->library('upload', $config);
    
    if ( ! $this->upload->do_upload())
    {
    echo 'error uploading file';
    }else{
        $filename=$this->uri->segment(3);
        $img_data=$this->upload->data();
            $new_imgname=$filename.'.jpg';
            $new_imgpath=$img_data['file_path'].$new_imgname;
            rename($img_data['full_path'], $new_imgpath);
            
            //$data['picture']=$id.'pics.jpg';
    }
    $this->othersview();
    
    }
    

    function eventheader(){
    $config['upload_path'] = './events/';
    $config['allowed_types'] = 'jpg';
    $config['file_name'] = 'header';
    $config['overwrite']=true;
     
    $this->load->library('upload', $config);
    
    if ( ! $this->upload->do_upload())
    {
    echo 'error uploading file';
    }else{
    
        $img_data=$this->upload->data();
            $new_imgname='header.jpg';
            $new_imgpath=$img_data['file_path'].$new_imgname;
            rename($img_data['full_path'], $new_imgpath);
            
            //$data['picture']=$id.'pics.jpg';
    }
    $this->eventview();
    }

function uploadslider(){
     $data=$this->get_form_data();

      $config['upload_path'] = './shop/';
    $config['allowed_types'] = 'jpg';
    $config['file_name'] = 'pics';
    $config['overwrite']=true;
     
    $this->load->library('upload', $config);
    
    if ( ! $this->upload->do_upload())
    {
    echo 'error uploading file';
    }else{
    
        $img_data=$this->upload->data();
            $new_imgname=$data['sliderimage'].'.jpg';
            $new_imgpath=$img_data['file_path'].$new_imgname;
            rename($img_data['full_path'], $new_imgpath);
            
            //$data['picture']=$id.'pics.jpg';
    }
    $this->shopview();
}

    function shopdelete(){
      $this->load->model('mdl_adminusers');
      $this->load->library('session');
      $this->mdl_adminusers->tablename="shop";
      $id = $this->uri->segment(3);
      $this->mdl_adminusers->_delete($id);
      redirect(base_url('adminusers/shopview'));
    }

    function shopedit(){
      $this->load->model('mdl_adminusers');
      $this->load->library('session');
      $this->mdl_adminusers->tablename="shop";
      $id = $this->uri->segment(3);
      $data=$this->get_form_data();

    $config['upload_path'] = './shop/';
    $config['allowed_types'] = '*';
    //$config['file_name'] = $id.'pics';
    $config['overwrite']=true;
     
    $this->load->library('upload', $config);
    



    if ( !($this->upload->do_upload()))
    {
    print_r($this->upload->display_errors());
    }else{
          foreach ($this->upload->data() as $key=>$value) {
            if($key=="orig_name"){
              $data['shopimage']=$value;
            }
          }

         
         
          
    }

     $this->mdl_adminusers->tablename='shop';
     $this->mdl_adminusers->_update($id,$data);
     redirect('adminusers/shopview');
    }
    function programview(){
      $this->load->library('session');
      $this->load->model('mdl_adminusers');
      $this->mdl_adminusers->tablename="programs";
       $query = $this->mdl_adminusers->get('id');
       $data['query']=$query;
      $this->load->view('adminusers',$data);
    }

    function shopview(){
      $this->load->library('session');
      $this->load->model('mdl_adminusers');
      $this->mdl_adminusers->tablename="shop";
       $query = $this->mdl_adminusers->get('id');
       $data['query']=$query;
      $this->load->view('shopping',$data);

    }

    function createclient(){
    $this->load->library('session');
    $data=$this->get_form_data();
    $this->load->model('mdl_adminusers');
    $data['shop_id']=$this->uri->segment(3);
      
    $config['upload_path'] = './shopimages/';
    $config['allowed_types'] = '*';
    //$config['max_size'] = '100';
    //$config['file_name'] = $id.'pics.jpg';
    $config['overwrite']=true;
    $this->load->library('upload', $config);
    
    if ( ! $this->upload->do_upload())
    {
    print_r($this->upload->display_errors());
    }else{
          foreach ($this->upload->data() as $key=>$value) {
            if($key=="orig_name"){
              $data['logo']=$value;
            }
          }

          $this->mdl_adminusers->tablename='shopclients';
          $this->mdl_adminusers->_insert($data);
          redirect('adminusers/shopclients/'.$data['shop_id']);
          
    }
     
    
    }



    function clientedit(){
    $this->load->library('session');
    $data=$this->get_form_data();
    $this->load->model('mdl_adminusers');
    $id=$this->uri->segment(3);
    $data['shop_id']=$this->uri->segment(4);
      
    $config['upload_path'] = './shopimages/';
    $config['allowed_types'] = '*';
    //$config['max_size'] = '100';
    //$config['file_name'] = $id.'pics.jpg';
    $config['overwrite']=true;
    $this->load->library('upload', $config);
    
    if ( !($this->upload->do_upload()))
    {
    print_r($this->upload->display_errors());
    }else{
          foreach ($this->upload->data() as $key=>$value) {
            if($key=="orig_name"){
              $data['logo']=$value;
            }
          }

         
         
          
    }

     $this->mdl_adminusers->tablename='shopclients';
     $this->mdl_adminusers->_update($id,$data);
     redirect('adminusers/shopclients/'.$data['shop_id']);
     
    }

    function shopclients(){
      $shop_id=$this->uri->segment(3);
      $this->load->model('mdl_adminusers');
      $this->mdl_adminusers->tablename='shopclients';
      $query=$this->mdl_adminusers->get_where_custom('shop_id',$shop_id);
      $data['shop_id']=$shop_id;
      $data['query']=$query;
      $this->load->view('clients',$data);

    }

    function createshop(){
      $this->load->model('mdl_adminusers');
      $this->load->library('session');
      $this->mdl_adminusers->tablename="shop";
      $data= $this->get_form_data();

       $config['upload_path'] = './shop/';
       $config['allowed_types'] = '*';
    //$config['max_size'] = '100';
    //$config['file_name'] = $id.'pics.jpg';
      $config['overwrite']=true;
      $this->load->library('upload', $config); 

      if ( ! $this->upload->do_upload())
    {
    print_r($this->upload->display_errors());
    }else{
          foreach ($this->upload->data() as $key=>$value) {
            if($key=="orig_name"){
              $data['shopimage']=$value;
            }
          }

          $this->mdl_adminusers->tablename='shop';
          $this->mdl_adminusers->_insert($data);
          redirect(base_url('adminusers/shopview'));
          
    }
      
      
    }

    

    function register(){
      $this->load->view('newuser');
    }
    function registeruser(){
      $this->load->model('mdl_adminusers');
      $this->mdl_adminusers->tablename="clients";
       $data = $this->get_form_data();
       $this->mdl_adminusers->_insert($data);
       redirect('adminusers');
    }
    function login(){
      $this->load->model('mdl_adminusers');
       $this->load->library('session');
        $this->mdl_adminusers->tablename="users";
       $data = $this->get_form_data();
    if ($data['usertype'] != "Admin"){
      $this->loginuser();
    }else{
       $query=$this->mdl_adminusers->get('password');
      $bool=true;
    foreach ($query->result() as $key => $value) {
        
      if (($data['username']==$value->username   )  ){
        $data['query']=$query;
        $data['level']=$value->usertype;
       
        $this->session->set_userdata('username', $value->username);
        $this->session->set_userdata('usertype', $value->usertype);
        
        if ($value->usertype == "Admin"){
           $this->track();
           $bool=false;
         }else{
          redirect(base_url('adminusers/programview'));
        }
      }
    }
    if($bool) {
        $errormsg='invalid';
        $data['errormsg']=$errormsg;
        //redirect('adminusers',$data);}
      $this->load->view('users',$data);}
    }}
    function loginuser(){
       $data = $this->get_form_data();
      $this->load->model('mdl_adminusers');
      $this->mdl_adminusers->tablename="users";

      $query=$this->mdl_adminusers->get('password');
      $bool=true;
    foreach ($query->result() as $key => $value) {
        
      if (($data['username']==$value->username   )  ){
        $data['query']=$query;  
        $this->session->set_userdata('username', $value->username);
        
        
        $this->mdl_adminusers->tablename="items";
        $query=$this->mdl_adminusers->get('name');
        $data['query']=$query;
         $this->load->view('userview',$data);
        $bool=false;}
      }
    
    if($bool) {
        $errormsg='invalid';
        $data['errormsg']=$errormsg;
        //redirect('adminusers',$data);}
      $this->load->view('users',$data);}
    }

 



    function showuser(){
      $this->load->library('session');
       $this->load->model('mdl_adminusers');
        $query=$this->mdl_adminusers->get('username');
        $data['query']=$query;
      $this->load->view('adminusers',$data);
    }
    function logout(){
       $this->load->library('session');
      $this->session->sess_destroy();
      redirect('adminusers');
    }

    function adminn(){
      $this->load->view('users');
    }
    function track(){
       $this->load->library('session');
     // $this->load->module('bulkpaymentupload');
      echo modules::run('bulkpaymentupload/track');
    }




    function docupload(){
      echo modules::run('bulkpaymentupload/docupload');
    }

    function createvideo(){
      $this->load->model('mdl_adminusers');
      $this->load->library('session');
      $this->mdl_adminusers->tablename="programs";
      $data= $this->get_form_data();
      
      $this->mdl_adminusers->_insert($data);
      $query=$this->mdl_adminusers->get('id');
      $mydata['query']=$query;
      $this->load->view('adminusers',$mydata);
    }

    function delete(){
       $this->load->library('session');
       $this->load->model('mdl_adminusers');
        $this->mdl_adminusers->tablename="programs";
      $delete_id =$this->uri->segment(3);
      $this->mdl_adminusers->_delete($delete_id);
    
    $this->mdl_adminusers->tablename="programs";
     $query=$this->mdl_adminusers->get('id');
     $mydata['query']=$query;
       
     $this->load->view('adminusers',$mydata);
    }

    function submituser(){
       $this->load->library('session');
       $data = $this->input->post();
       $query=$this->mdl_adminusers->get_where('1');
       $adminpaswrd='';
      foreach($query->result() as $dbadmin){
        $adminpaswrd=$dbadmin->password;
      }
      if ($adminpaswrd !=md5($data['adminpassword'])){
       echo 'invalid admin password';
      }
     if ($this->checkPasswordExists($data['pass'])){
         echo 'Password alredy exists';
         exit;
     }
     
       $data['pass']=md5($data['pass']);
       $dbuser['username']=$data['user'];
       $dbuser['password']=$data['pass'];
       $this->mdl_adminusers->_insert($dbuser);
       echo 'user created successfully';
       
    }

    

       
  function get_form_data(){
    $data = $this->input->post();
    return $data;
}

 
    
}


/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

?>
