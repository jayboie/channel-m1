<?php

class post extends MX_Controller
{
 

function __construct() {
    parent::__construct();
}

function index(){    
    redirect('admin');
}
 

 function create(){


    $this->load->library('session');
    $data=$this->get_form_data();
    $this->load->model('mdl_post');
    $this->mdl_post->_insert($data);
    $data['query']=$this->mdl_post->get('id');
    $this->load->view('members',$data);
 } 

 function showinfections(){
     $this->load->model('mdl_post');
    $this->load->library('session');
    $this->mdl_post->tablename='infections';
    $data['query']=$this->mdl_post->get_with_limit(20,0,'id');
   
    $this->load->view('infections',$data);
 }

 function createinfection(){
    $data=$this->get_form_data();
    $data['infectionname']=strtolower($data['infectionname']);
    $this->load->model('mdl_post');
    $this->mdl_post->tablename='infections'; 
    $query=$this->mdl_post->get('id');
    
    $id=0;
    foreach ($query->result() as  $value) {
        $id=$value->id;
    }
    $id =$id+1;

    $config['upload_path'] = './Infections/';
    $config['allowed_types'] = 'jpg|png|jpeg|bmp';
    $config['file_name'] = $id.'pics.jpg';
    $config['overwrite']=true;
    $this->load->library('upload', $config);
    
    if ( ! $this->upload->do_upload())
    {
    echo 'error uploading file';
    }else{
            $data['picname']=$id.'pics.jpg';
    }

   
   
   $this->mdl_post->_insert($data);
   redirect(base_url('post/showinfections'));
}

function createpost(){
    $data=$this->get_form_data();
    $this->load->model('mdl_post');
    $this->mdl_post->tablename='posts'; 
    $query=$this->mdl_post->get('id');
    
    $id=0;
    foreach ($query->result() as  $value) {
        $id=$value->id;
    }
    $id =$id+1;

    $config['upload_path'] = './Postpics/';
    $config['allowed_types'] = 'jpg|png|jpeg|bmp';
    $config['file_name'] = $id.'pics.jpg';
    $config['overwrite']=true;
    $this->load->library('upload', $config);
    
    if ( ! $this->upload->do_upload())
    {
    echo 'error uploading file';
    }else{
            $data['picname']=$id.'pics.jpg';
    }

   
   
   $this->mdl_post->_insert($data);
   redirect(base_url('post/showposts'));
}

function createevent(){
    $data=$this->get_form_data();
    $this->load->model('mdl_post');
    $this->mdl_post->tablename='event'; 
    $query=$this->mdl_post->get('id');
    
    $id=0;
    foreach ($query->result() as  $value) {
        $id=$value->id;
    }
    $id =$id+1;

    $config['upload_path'] = './Eventpics/';
    $config['allowed_types'] = 'jpg|png|jpeg|bmp';
    $config['file_name'] = $id.'pics.jpg';
    $config['overwrite']=true;
    $this->load->library('upload', $config);
    
    if ( ! $this->upload->do_upload())
    {
    echo 'error uploading file';
    }else{
            $data['picname']=$id.'pics.jpg';
    }

   
   
   $this->mdl_post->_insert($data);
   redirect(base_url('post/showevents'));
}

 function showposts(){
    $this->load->model('mdl_post');
    $this->load->library('session');
    $this->mdl_post->tablename='posts';
    $data['query']=$this->mdl_post->get('id');
   
    $this->load->view('posts',$data);
}



function showevents(){
    $this->load->model('mdl_post');
    $this->load->library('session');
    $this->mdl_post->tablename='event';
    $data['query']=$this->mdl_post->get('id');

    $this->load->view('events',$data);
}


function editpost(){

    $id=$this->uri->segment(3);
    $data=$this->get_form_data(); 

    $config['upload_path'] = './Postpics/';
    $config['allowed_types'] = '*';
    $config['file_name'] = $id.'pics.jpg';
    $config['overwrite']=true;
    $this->load->library('upload', $config);
    
    if ( ! $this->upload->do_upload())
    {
    echo 'error uploading file';
    }else{
            $data['picname']=$id.'pics.jpg';
    }
    $this->load->model('mdl_post');
    $this->mdl_post->gettablename('posts');
    $this->mdl_post->_update($id,$data);
    redirect(base_url('post/showposts'));
}


function editevent(){

    $id=$this->uri->segment(3);
    $data=$this->get_form_data();
   // print_r($data['userfile']);

    $config['upload_path'] = './Eventpics/';
    $config['allowed_types'] = '*';
    $config['file_name'] = $id.'pics.jpg';
    $config['overwrite']=true;
    $this->load->library('upload', $config);
    
    if ( ! $this->upload->do_upload())
    {
    echo 'error uploading file';
    }else{
            $data['picname']=$id.'pics.jpg';
    }
    $this->load->model('mdl_post');
    $this->mdl_post->gettablename('event');
    $this->mdl_post->_update($id,$data);
    redirect(base_url('post/showevents'));
}



function editinfection(){

    $id=$this->uri->segment(3);
    $data=$this->get_form_data();
   // print_r($data['userfile']);

    $config['upload_path'] = './Infections/';
    $config['allowed_types'] = '*';
    $config['file_name'] = $id.'pics.jpg';
    $config['overwrite']=true;
    $this->load->library('upload', $config);
    
    if ( ! $this->upload->do_upload())
    {
    echo 'error uploading file';
    }else{
            $data['picname']=$id.'pics.jpg';
    }
    $this->load->model('mdl_post');
    $this->mdl_post->gettablename('infections');
    $this->mdl_post->_update($id,$data);
    redirect(base_url('post/showinfections'));
}


 function showdetails(){
    $this->load->model('mdl_post');
    $this->load->library('session');
    $this->mdl_post->tablename='products';
    $cars=$this->mdl_post->get('id');


    $this->mdl_post->tablename='details';
    $data['query']=$this->mdl_post->get_with_limit(20,0,'id');
   
    $this->load->view('cars',$data);
 }

function showusers(){
    $this->load->library('session'); 
    $this->load->model('mdl_post');
    $this->mdl_post->gettablename('users');  
    $query=$this->mdl_post->get('id');   
    $data['query']=$query;
    $this->load->view('users',$data);
} 

 
 

function deletepost(){
    $id=$this->uri->segment(3);
    $this->load->model('mdl_post');
    $this->mdl_post->gettablename('posts');
    $this->mdl_post->_delete($id);
   redirect(base_url('post/showposts'));
}

function deleteevent(){
    $id=$this->uri->segment(3);
    $this->load->model('mdl_post');
    $this->mdl_post->gettablename('event');
    $this->mdl_post->_delete($id);
   redirect(base_url('post/showevents'));
}



function deletedownload(){
    $id=$this->uri->segment(3);
    $this->load->model('mdl_post');
    $this->load->library('session');
    $this->mdl_post->tablename='downloads';
    $this->mdl_post->_delete($id);
    redirect(base_url('product/showdownloads'));
}

function addDownload(){
     $this->load->model('mdl_post');
    $this->load->library('session');
    $this->mdl_post->tablename='downloads';
    $data=$this->get_form_data();
    $this->mdl_post->_insert($data);
}

function showdownloads(){
    $this->load->model('mdl_post');
    $this->load->library('session');
    $this->mdl_post->tablename='downloads';
    $data['query']=$this->mdl_post->get('id');
    $this->load->view('downloads',$data);

}



function uploadapp(){
    $data=$this->get_form_data();
    $this->load->model('mdl_post');
    $id=$this->uri->segment(3);
    $this->mdl_post->tablename='apps'; 
     

    $config['upload_path'] = './Apk/';
    $config['allowed_types'] = '*';
    $config['file_name'] = $data['appname'].'apk';
    $config['overwrite']=true;
    $this->load->library('upload', $config);
    
    
    if ( ! $this->upload->do_upload())
    {
    echo 'error uploading file';
    $this->upload->display_errors();
    }else{ 

            $this->mdl_post->_update($id,$data);

           // $data['picname']=$id.'pics.jpg';
        redirect(base_url('product/showposts'));
    }

   
   
  // $this->mdl_post->_insert($data);
  // redirect(base_url('product/showbooks'));
}



function doctortip($data){
    $this->load->model('mdl_post');
    $this->mdl_post->gettablename('learn'); 
    $query=$this->mdl_post->get('id');
 
    $id=0;
    foreach ($query->result() as  $value) {
        $id=$value->id;
    }
    $id =$id+1;

    $config['upload_path'] = './Learn/';
    $config['allowed_types'] = 'jpg';
    $config['file_name'] = $id.'pics';
    $config['overwrite']=true;
    $this->load->library('upload', $config);
    
    if ( ! $this->upload->do_upload())
    {
    echo 'error uploading file';
    }else{
            $data['picture']=$id.'pics.jpg';
    }
    $this->load->model('mdl_post');
    $this->mdl_post->gettablename('learn'); 
   $this->mdl_post->_insert($data);
   //$this->learn();
}

function logout(){
    redirect('adminusers');
}

function adminuser(){
  echo base_url('');
   // redirect('adminusers');
}



 

function get_form_data(){
    $data = $this->input->post();
    return $data;
}

 
}

?>