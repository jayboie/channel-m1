 <?php if (session_status() == PHP_SESSION_NONE) {
    session_start();
}?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>One Credit Application Administration<?php echo " | ".$title; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="One Credit Application Administration">
	<meta name="author" content="Olanipekun Olufemi, KVPAfrica">

	<!-- The styles -->
	 
	<style type="text/css">
	  body {
		padding-bottom: 40px;
	  }
	  .sidebar-nav {
		padding: 9px 0;
	  }
	</style>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/boomtstrap.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fullcalendar.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fullcalendar.print.css'); ?>">
	 <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/bootstrap-cerulean.css'); ?>">
	  <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/charisma-app.css'); ?>">
	    <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/bootstrap-responsive.css'); ?>">
	  <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/jquery-ui-1.8.21.custom.css'); ?>">
	    <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/chosen.css'); ?>">

	    <link rel="stylesheet" href="<?php echo base_url('assets/admin/css//uniform.default.css'); ?>">
	    <link rel="stylesheet" href="<?php echo base_url('assets/admin/css//colorbox.css'); ?>">
	  <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/jquery.cleditor.css'); ?>">
	    <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/jquery.noty.css'); ?>">
 
 	<link rel="stylesheet" href="<?php echo base_url('assets/admin/css/noty_theme_default.css'); ?>">
	    <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/elfinder.min.css'); ?>">
	  <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/elfinder.theme.css'); ?>">
	    <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/opa-icons.css'); ?>">
	 
	 

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- The fav icon -->
	 
	
    <!-- jQuery -->
    <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	 
</head>

<body>
	 
	<!-- topbar starts -->
	 <div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				 
				<a class="brand" href="#">Bulk Payment Upload</a>
				
				<!-- user dropdown starts -->
				<div class="btn-group pull-right">
					<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="icon-user"></i> <span> <?php echo $_SESSION['username'];?></span><span class="hidden-phone"> </span>
						<span class="caret"></span>

					</a>
					<ul class="dropdown-menu">

						<li><a href="<?php echo base_url('Adminusers'); ?>">Logout</a></li>
					</ul>
				</div>
				<!-- user dropdown ends -->
				
				<div class="top-nav nav-collapse">
					<ul class="nav">
						<li><a href="http://www.one-cred.com">Visit Site</a></li>
						 
					</ul>
				</div><!--/.nav-collapse -->
			</div>
		</div>
	</div>


	<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- left menu starts -->
			<div class="span2 main-menu-span">
				<div class="well nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">
						<li class="nav-header hidden-tablet">Admin Menu</li>
						
        				<li id="ld" style="margin-left: -2px;"><a class="ajax-link" href="<?php echo base_url('bulkpaymentupload/docupload/');?>"><i class="icon-upload"></i><span class="hidden-tablet">Upload</span></a></li>
	 					<li  class="active" style="margin-left: -2px;"><a class="ajax-link" href="#"><i class="icon-eye-open"></i><span class="hidden-tablet">Report</span></a></li>
      
     <?php
                        if ($_SESSION['usertype']=="Superadmin"){
                       		  	
                       		echo '<li id="ld" style="margin-left: -2px;"><a  href="'. base_url('adminusers/showuser').'"><i class="icon-user"></i><span class="hidden-tablet">Adminusers</span></a></li>';
                        } 
                        ?>
       
<label id="for-is-ajax" class="hidden-tablet" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label>
				</ul></div><!--/.well -->
			</div><!--/span-->
			<!-- left menu ends -->
			
			 
			<div id="content" class="span10">
			<!-- content starts -->
			        <div class="alerts">
                </div>
        
<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>  
        </li>
        <li>
            <a href="#">Track</a>
        </li>
 
    </ul>
</div>

<?php
$count=0;
$lcount=0;
foreach ($query->result() as $value) {
					  		 
					  		if ($value->status=="Successful"){
					  			$count ++;
					  		}else{
					  			$lcount ++;
					  		}
					  	}
?>

<div class="row-fluid sortable ui-sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title="" >
            <h2><i class="icon-file"></i>Upload Reports</h2>
             <h2 class="pull-right"> <?php echo $count; ?> successful upload</h2>
        </div>
        
        <div class="box-content">
            <div class="tabbable"> <!-- Only required for left/right tabs -->
              <ul class="nav nav-tabs">
                <li class="active"><a href="#tab1" data-toggle="tab">All Upload</a></li>
                <li ><a href="#tab2" data-toggle="tab">By Date</a></li>
                 <li ><a href="#tab3" data-toggle="tab">By Description</a></li>
                   <li ><a href="#tab4" data-toggle="tab">User actions</a></li>
                
              </ul>
              
              <div class="tab-content">
               
                <div class="tab-pane active"  id="tab1">
                <?php if (isset($succes)){?>
             <a href="<?php if (isset($succes)){echo base_url('assets/'.$succes.'.csv');}?>">Success file</a>
                 <a href="<?php if (isset($failed)){echo base_url('assets/'.$failed.'.csv');}?>"> Failed file</a> <?php }?>
                  <div class="box-content" style="display: block;">
						<div class="box-content">
							<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid">
							<table class="table table-striped table-bordered bootstrap-datatable datatable dataTable" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info">
						 
						  <thead>
							  <tr role="row">
							  <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending" style="width: 15px;">S/N</th> 	
							  <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending" style="width: 211px;">CLIENT ID</th>
							  <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 175px;">FULLNAME</th>
							  <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Actions: activate to sort column ascending" style="width: 391px;">AMOUNT</th>
							   <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 175px;">BANKNAME</th>
							    <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 175px;">CHECKNO</th>
							  <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Actions: activate to sort column ascending" style="width: 391px;">DATE</th>
							  <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Actions: activate to sort column ascending" style="width: 391px;">EMPLOYEENO</th>
							  <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Actions: activate to sort column ascending" style="width: 391px;">DESCRIPTION</th>
							  <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Actions: activate to sort column ascending" style="width: 391px;">STATUS</th>
							  </tr>
						  </thead>   
						  
					  <tbody role="alert" aria-live="polite" aria-relevant="all">

					  <?php 

					  $c=0;
					  	foreach ($query->result() as $value) {
					  		$c+=1;
					  		$value->amount = number_format($value->amount,2);
					  		$color="";
					  		if ($value->status=="Successful"){
					  			$color="success";
					  		}else{
					  			$color="danger";
					  		}
					  		if ($c%2==0){
					  			echo '<tr class="even">';
					  		}else{
					  			echo '<tr class="odd">';
					  		}
					  		echo '<td class=" sorting_1">'.$c.'</td>
					  		<td class=" sorting_1">'.$value->clientid.'</td><td class="sorting_1 " >'.$value->fullname.'</td>
								<td class="sorting_1 ">&#x20a6;'.$value->amount.'</td>
								<td class="sorting_1 ">'.$value->bankname.'</td>
								<td class="sorting_1 ">'.$value->checknumber.'</td>
								<td class="sorting_1 ">'.$value->date.'</td>
								<td class="sorting_1 ">'.$value->employeeno.'</td>
								<td class="sorting_1 ">'.$value->paydescription.'</td>
								<td class="center "><a class="btn btn-'.$color.'" href="#">'.$value->status.'</a></td>
							</tr>';
					  	}

					  ?>
					  
						</tbody>
				</table>
		</div> 

						</div>
					</div>
                &nbsp;&nbsp;&nbsp;&nbsp;<span style="align:center; color: green;font-size: 20px;"></span><br>                
              </div>

              <div class="tab-pane"  id="tab2">
                
               <form action="<?php echo base_url('bulkpaymentupload/date/'.$username); ?>" method="post" enctype="multipart/form-data">
                <div class="control-group">
							  <label class="control-label" for="date01">From date</label>
							  <div class="controls">
								<input type="text" class="input-xlarge datepicker" name="date1" id="date01"  placeholder="select date">
							  </div>
				</div>

				<div class="control-group">
							  <label class="control-label" for="date01">To date</label>
							  <div class="controls">
								<input type="text" class="input-xlarge datepicker" name="date2" id="date02"  placeholder="select date"> 
							  </div>
				</div>
               
				<input type="submit"  name="submit" value="Upload" class="btn btn-primary"/>

				</form>

				<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid">
							<table class="table table-striped table-bordered bootstrap-datatable datatable dataTable" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info">
						 
						  <thead>
							 <tr role="row">
							 <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending" style="width: 15px;">S/N</th>
							 <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending" style="width: 211px;">CLIENT ID</th>
							  <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 175px;">FULLNAME</th>
							  <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Actions: activate to sort column ascending" style="width: 391px;">AMOUNT</th>
							   <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 175px;">BANKNAME</th>
							    <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 175px;">CHECKNO</th>
							  <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Actions: activate to sort column ascending" style="width: 391px;">DATE</th>
							  <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Actions: activate to sort column ascending" style="width: 391px;">EMPLOYEE NO</th>
							  <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Actions: activate to sort column ascending" style="width: 391px;">DESCRIPTION</th>
							  <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Actions: activate to sort column ascending" style="width: 391px;">STATUS</th>
							  </tr>
						  </thead>   
						  
					  <tbody role="alert" aria-live="polite" aria-relevant="all">

					  <?php 

					  $c=0;
					  
					  	//print_r($descripdb);
					  if(isset($datedb) and ($datedb != "")){
					  	echo'<a class="btn btn-success" href="'.base_url('assets/'.$from.' to '.$to.' success.csv').'">
							success csv	<i class="icon-download icon-white"></i></a>
							<a class="btn btn-danger" href="'.base_url('assets/'.$from.' to '.$to.' failed.csv').'">
										failed csv<i class="icon-download icon-white"></i></a>';


					  	for($index=0; $index<count($datedb); $index++){
					  	foreach ($datedb[$index]->result() as $value) {
					  		 
					  		$c+=1;
					  		$color="";
					  		if ($value->status=="Successful"){
					  			$color="success";
					  		}else{
					  			$color="danger";
					  		}
					  		if ($c%2==0){
					  			echo '<tr class="even">';
					  		}else{
					  			echo '<tr class="odd">';
					  		}
					  		echo '<td class=" sorting_1">'.$c.'</td>
					  		<td class=" sorting_1">'.$value->clientid.'</td>
								 
								<td class="sorting_1 " >'.$value->fullname.'</td>
								<td class="sorting_1 ">#'.$value->amount.'</td>
								<td class="sorting_1 ">'.$value->bankname.'</td>
								<td class="sorting_1 ">'.$value->checknumber.'</td>
								<td class="sorting_1 ">'.$value->date.'</td>
								<td class="sorting_1 ">'.$value->employeeno.'</td>
								<td class="sorting_1 ">'.$value->paydescription.'</td>
								<td class="sorting_1 "> 
									<a class="btn btn-'.$color.'" href="#">'.$value->status.'</a>                                        
									
									
									
								</td>
							</tr>';
					  	}
					  }
					}

					  ?>
					  
						</tbody>
				</table>
		</div>


				 

               </div>

               <div class="tab-pane"  id="tab3">
                <form action="<?php echo base_url('bulkpaymentupload/description/'.$username); ?>" method="post" enctype="multipart/form-data">
                 <div class="control-group">
				<label class="control-label" for="selectError2">Select description</label>
				<div class="controls">
				<select name="dbdescription" data-placeholder="Your Upload description" id="selectError2" data-rel="chosen">
				<option value=""></option>
                	<?php
                		if (isset($desc)){
                			foreach ($desc->result() as $value) {
                				echo "<option>".$value->description."</option>";
                			}
                		}
                	?>
                </select>
                </div>
                <input type="submit"  name="submit" value="Upload" class="btn btn-primary"/>
			</div>
				
				</form>


					 <?php if (isset($succes)){?>
             <a href="<?php if (isset($succes)){echo base_url('assets/'.$succes.'.csv');}?>">Success file</a>
                 <a href="<?php if (isset($failed)){echo base_url('assets/'.$failed.'.csv');}?>"> Failed file</a> <?php }?>

							<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid">
							<table class="table table-striped table-bordered bootstrap-datatable datatable dataTable" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info">
						 
						  <thead>
							  <tr role="row">
							  <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending" style="width: 15px;">S/N</th>
							  <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending" style="width: 211px;">CLIENT ID</th>
							  <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 175px;">FULLNAME</th>
							  <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Actions: activate to sort column ascending" style="width: 391px;">AMOUNT</th>
							   <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 175px;">BANKNAME</th>
							    <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 175px;">CHECKNO</th>
							  <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Actions: activate to sort column ascending" style="width: 391px;">DATE</th>
							  <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Actions: activate to sort column ascending" style="width: 391px;">EMPLOYEE NO</th>
							  <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Actions: activate to sort column ascending" style="width: 391px;">DESCRIPTION</th>
							  <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Actions: activate to sort column ascending" style="width: 391px;">STATUS</th>
							  </tr>
						  </thead>   
						  
					  <tbody role="alert" aria-live="polite" aria-relevant="all">

					  <?php 

					  $c=0;
					  
					  	//print_r($descripdb);
					  if(isset($descripdb)){
					  	foreach ($descripdb->result() as $value) {
					  		 
					  		$c+=1;
					  		$color="";
					  		if ($value->status=="Successful"){
					  			$color="success";
					  		}else{
					  			$color="danger";
					  		}
					  		if ($c%2==0){
					  			echo '<tr class="even">';
					  		}else{
					  			echo '<tr class="odd">';
					  		}
					  		echo '<td class=" sorting_1">'.$c.'</td>
					  		<td class=" sorting_1">'.$value->clientid.'</td>
								 
								<td class="sorting_1 " >'.$value->fullname.'</td>
								<td class="sorting_1 ">#'.$value->amount.'</td>
								<td class="sorting_1 ">'.$value->bankname.'</td>
								<td class="sorting_1 ">'.$value->checknumber.'</td>
								<td class="sorting_1 ">'.$value->date.'</td>
								<td class="sorting_1 ">'.$value->employeeno.'</td>
								<td class="sorting_1 ">'.$value->paydescription.'</td>
								<td class="center "> 
									<a class="btn btn-'.$color.'" href="#">
										<i class="icon-edit icon-white"></i>  
										'.$value->status.'                                        
									</a>
									
									
								</td>
							</tr>';
					  	}
					  }

					  ?>
					  
						</tbody>
				</table>
		</div>


               </div>
               
               <div class="tab-pane"  id="tab4">
               
							<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid">
							<table class="table table-striped table-bordered bootstrap-datatable datatable dataTable" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info">
						 
						  <thead>
							  <tr role="row">
							  <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"  aria-label="Username: activate to sort column descending" style="width: 15px;">S/N</th>	
							  <th role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"  aria-label="Username: activate to sort column descending" style="width: 211px;">USERNAME</th>
							  <th  role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="descending" aria-label="Date registered: activate to sort column descending" style="width: 175px;">TIME OF ACTION</th>
							  <th  role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Actions: activate to sort column ascending" style="width: 391px;">REPORT</th>
							
					
							  </tr>
						  </thead>   
						  
					  <tbody role="alert" aria-live="polite" aria-relevant="all">

					  <?php 

					  $c=0;
					  
					  	//print_r($descripdb);

					  if(isset($desc)){
					  	foreach ($desc->result() as $value) {
					  		 
					  		 $mytracks = explode('/',$value->trackaction );
					  		  $sucesurl= base_url('assets/'.$value->id.'sucess.csv');
					  		  $failedurl= base_url('assets/'.$value->id.'failed.csv');
					  		$c+=1;
					  		 
					  		 if (count($mytracks) <= 1){
					  		 	$mytracks[0]='not set';
					  		 	$mytracks[1]='not set';
					  		 }
					  		if ($c%2==0){
					  			echo '<tr class="even">';
					  		}else{
					  			echo '<tr class="odd">';
					  		}
					  		echo '<td class=" sorting_1">'.$c.'</td>
					  		<td class=" sorting_1">'.$mytracks[0].'</td>
								 
								<td class="sorting_1 " >'.$mytracks[1].'</td>
								<td>
								<a class="btn btn-success" href="'.$sucesurl.'">
									success csv  <i class="icon-download icon-white"></i>                                
									</a>
								<a class="btn btn-danger" href="'.$failedurl.'">
										failed csv <i class="icon-download icon-white"></i>                             
									</a>
								 </td>
							</tr>';
					  	}
					  }

					  ?>
					  
						</tbody>
				</table>
		</div>


               </div>
              </div>
            </div>   
        </div>
    </div>
</div>
        					<!-- content ends -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
				
		<hr>

		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">�</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>

		<footer>
			<p class="pull-left">© <a href="http://one-cred.com" target="_blank">One Credit</a> 2014</p>
		</footer>
		
	</div>






	<!-- jQuery -->
	
	<script src="<?php echo base_url('assets/admin/js/jquery-1.7.2.min.js');?>"></script>

	<!-- jQuery UI -->
	<script src="<?php echo base_url('assets/admin/js/jquery-ui-1.8.21.custom.min.js');?>"></script>
	<!-- transition / effect library -->
	<script src="<?php echo base_url('assets/admin/js/bootstrap-transition.js');?>"></script>
	<!-- alert enhancer library -->
	<script src="<?php echo base_url('assets/admin/js/bootstrap-alert.js');?>"></script>
	<!-- modal / dialog library -->
	<script src="<?php echo base_url('assets/admin/js/bootstrap-modal.js');?>"></script>
	<!-- custom dropdown library -->
	<script src="<?php echo base_url('assets/admin/js/bootstrap-dropdown.js');?>"></script>
	<!-- scrolspy library -->
	<script src="<?php echo base_url('assets/admin/js/bootstrap-scrollspy.js');?>"></script>
	<!-- library for creating tabs -->
	<script src="<?php echo base_url('assets/admin/js/bootstrap-tab.js');?>"></script>
	<!-- library for advanced tooltip -->
	<script src="<?php echo base_url('assets/admin/js/bootstrap-tooltip.js');?>"></script>
	<!-- popover effect library -->
	<script src="<?php echo base_url('assets/admin/js/bootstrap-popover.js');?>"></script>
	<!-- button enhancer library -->
	<script src="<?php echo base_url('assets/admin/js/bootstrap-button.js');?>"></script>
	<!-- accordion library (optional, not used in demo) -->
	<script src="<?php echo base_url('assets/admin/js/bootstrap-collapse.js');?>"></script>
	<!-- carousel slideshow library (optional, not used in demo) -->
	<script src="<?php echo base_url('assets/admin/js/bootstrap-carousel.js');?>"></script>
	<!-- autocomplete library -->
	<script src="<?php echo base_url('assets/admin/js/bootstrap-typeahead.js');?>"></script>
	<!-- tour library -->
	<script src="<?php echo base_url('assets/admin/js/bootstrap-tour.js');?>"></script>
	<!-- library for cookie management -->
	<script src="<?php echo base_url('assets/admin/js/jquery.cookie.js');?>"></script>
	<!-- calander plugin -->
	<script src="<?php echo base_url('assets/admin/js/fullcalendar.min.js'); ?>"></script>
	<!-- data table plugin -->
	<script src="<?php echo base_url('assets/admin/js/jquery.dataTables.min.js');?>"></script>

	<!-- chart libraries start -->
	<script src="<?php echo base_url('assets/admin/js/excanvas.js');?>"></script>
	<script src="<?php echo base_url('assets/admin/js/jquery.flot.min.js');?>"></script>
	<script src="<?php echo base_url('assets/admin/js/jquery.flot.pie.min.js');?>"></script>
	<script src="<?php echo base_url('assets/admin/js/jquery.flot.stack.js');?>"></script>
	<script src="<?php echo base_url('assets/admin/js/jquery.flot.resize.min.js');?>"></script>
	<!-- chart libraries end -->

	<!-- select or dropdown enhancer -->
	<script src="<?php echo base_url('assets/admin/js/jquery.chosen.min.js');?>"></script>
	<!-- checkbox, radio, and file input styler -->
	<script src="<?php echo base_url('assets/admin/js/jquery.uniform.min.js');?>"></script>
	<!-- plugin for gallery image view -->
	<script src="<?php echo base_url('assets/admin/js/jquery.colorbox.min.js');?>"></script>
	<!-- rich text editor library -->
	<script src="<?php echo base_url('assets/admin/js/jquery.cleditor.min.js');?>"></script>
	<!-- notification plugin -->
	<script src="<?php echo base_url('assets/admin/js/jquery.noty.js');?>"></script>
	<!-- file manager library -->
	<script src="<?php echo base_url('assets/admin/js/jquery.elfinder.min.js');?>"></script>
	<!-- star rating plugin -->
	<script src="<?php echo base_url('assets/admin/js/jquery.raty.min.js');?>"></script>
	<!-- for iOS style toggle switch -->
	<script src="<?php echo base_url('assets/admin/js/jquery.iphone.toggle.js');?>"></script>
	<!-- autogrowing textarea plugin -->
	<script src="<?php echo base_url('assets/admin/js/jquery.autogrow-textarea.js');?>"></script>
	<!-- multiple file upload plugin -->
	<script src="<?php echo base_url('assets/admin/js/jquery.uploadify-3.1.min.js');?>"></script>
	<!-- history.js for cross-browser state change on ajax -->
	<script src="<?php echo base_url('assets/admin/js/jquery.history.js');?>"></script>
	<!-- application script for Charisma demo -->
	<script src="<?php echo base_url('assets/admin/js/charisma.js');?>"></script>
	
	<script>
	$("#ld").click(function(){
		 
	})
	</script>
	</body>
	</html>